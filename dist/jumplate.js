"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/************ Jumplate ************/
(function (exports, isNode) {
	var $fs, $path;

	if (isNode) {
		$fs = require("fs");
		$path = require("path");
	}

	var SPEC_CHAR_REGEX = /[\x26\x3c\x22\x27\x3e]/g;

	/**
  * Class which load Jumplate's template code and compile it.
  * Compiled template can be taken and saved for future use.
  * After compile then it can be rendered into HTML with given set of data.
  */

	var Jumplate = function () {

		//<editor-fold desc="Constructor">

		/**
   * Construct Jumplate; You should let Jumplate loads template files on its own
   * if you call constructor like new Jumplate(null, "path") or new Jumplate(null, "path", null, "layout path")
   * @param {String} template Template content
   * @param {String} [templatePath] Path to template, used for resolving includes
   * @param {String} [layout] Layout template content
   * @param {String} [layoutPath] Path to layout template, used for resolving includes
   * @throws {Error}
   */
		function Jumplate(template) {
			var templatePath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			var layout = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
			var layoutPath = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

			_classCallCheck(this, Jumplate);

			/**
    * @private
    * @type {null}
    */
			this.localizator = null;

			/**
    * @private
    * @type {null}
    */
			this.helpers = {};

			// If it's called from static method ::fromCache() - do not continue with initialization
			if (arguments.length === 0) {
				return;
			}

			// For client, only first two params allowed
			if (!isNode) {
				layout = templatePath;
				templatePath = null;
				layoutPath = null;
			}

			if ((template !== null || templatePath === null) && (typeof template != "string" || template.toString().trim().length == 0)) {
				throw new Error("Invalid template");
			}

			if (templatePath !== null && (typeof templatePath != "string" || templatePath.toString().trim().length == 0)) {
				throw new Error("Invalid template path");
			}

			if (layout !== null && (typeof layout != "string" || layout.toString().trim().length == 0)) {
				throw new Error("Invalid layout");
			}

			if (layoutPath !== null && (typeof layoutPath != "string" || layoutPath.toString().trim().length == 0)) {
				throw new Error("Invalid layout path");
			}

			if (templatePath != null) {
				templatePath = $path.resolve(templatePath);
			}

			if (layoutPath != null) {
				layoutPath = $path.resolve(layoutPath);
			}

			/**
    * @private
    * @type {String}
    */
			this.template = template;

			/**
    * @private
    * @type {String}
    */
			this.templatePath = templatePath;

			/**
    * @private
    * @type {String}
    */
			this.layout = layout;

			/**
    * @private
    * @type {String}
    */
			this.layoutPath = layoutPath;

			/**
    * Field for code of compiled template
    * @private
    * @type {string}
    */
			this.output = "";

			/**
    * Field for code of compiled template's layout
    * @private
    * @type {string}
    */
			this.layoutOutput = "";

			/**
    * Your optional context under which will be helpers called ("this" will point to context)
    * @type {Object}
    */
			this.context = null;
		}

		//</editor-fold>

		//<editor-fold desc="Virtual Callbacks">

		/**
   * Virtual callback for render() method
   * @callback renderCallback
   * @param {Error} error Error
   * @param {string | null} htmlOutput Output HTML
   */

		/**
   * Virtual callback for compile() method
   * @callback compileCallback
   * @param {Error} error Error
   * @param {string | null} template Compiled template code
   */

		//</editor-fold>

		//<editor-fold desc="Static Methods">

		/**
   * Returns instance of Jumplate with precompiled code, just ready for render()
   * @param {String} cachedTemplate Content of previously compiled template
   * @returns {Jumplate}
   */


		_createClass(Jumplate, [{
			key: "compile",


			//</editor-fold>

			//<editor-fold desc="Public Methods">

			/**
    * Compile input template
    * @param {compileCallback} callback
    */
			value: function compile(callback) {
				var _this = this;

				if (typeof this.template == "undefined") {
					// Template was created from cached data; No compile possible
					return;
				}

				var templateDone = false,
				    layoutDone = false,
				    noLayout = this.layoutPath == null && this.layout == null;
				// noLayout will change default BLOCK behavior; if layout set, block will not produce output
				// it'll wait for include

				var checkIfBothDone = function checkIfBothDone() {
					if (noLayout) {
						callback(null, _this.output);
						return;
					}

					if (templateDone && layoutDone) {
						var output = '(function(){let __output="";' + _this.output + '})();' + _this.layoutOutput;
						_this.output = output;
						callback(null, output);
					}
				};

				var someErrorOccursAlready = false;

				var templateFileCallback = function templateFileCallback(err, templateContent) {
					if (someErrorOccursAlready) return;

					if (err) {
						someErrorOccursAlready = true;
						err.message = "[Template Error] " + err.stack;
						callback(err, null);
						callback = function callback() {};
						return;
					}

					var parse = isNode ? $path.parse(_this.templatePath || "") : { dir: "", base: "" };

					// Kompilace šablony
					var parser = new Jumplate.Parser(templateContent, parse.dir, parse.base);
					parser.parse(function (err, template) {
						if (someErrorOccursAlready) return;

						if (err) {
							someErrorOccursAlready = true;
							err.message = "[Template Error] " + err.stack;
							callback(err, null);
							return;
						}

						_this.output = template;
						templateDone = true;

						checkIfBothDone();
					});
				};

				// Načte šablonu
				if (this.template == null) {
					$fs.readFile(this.templatePath, "utf8", templateFileCallback);
				} else {
					templateFileCallback(null, this.template);
				}

				// Načte layout
				if (!noLayout) {
					var layoutFileCallback = function layoutFileCallback(err, layoutContent) {
						if (someErrorOccursAlready) return;

						if (err) {
							someErrorOccursAlready = true;
							err.message = "[Layout Error] " + err.stack;
							callback(err, null);
							return;
						}

						var parse = isNode ? $path.parse(_this.templatePath || "") : { dir: "", base: "" };

						// Kompilace layoutu
						var layoutParser = new Jumplate.Parser(layoutContent, parse.dir, parse.base);
						layoutParser.parse(function (err, layout) {
							if (someErrorOccursAlready) return;

							if (err) {
								someErrorOccursAlready = true;
								err.message = "[Layout Error] " + err.stack;
								callback(err, null);
								return;
							}

							_this.layoutOutput = layout;
							layoutDone = true;

							checkIfBothDone();
						});
					};

					if (this.layout == null) {
						$fs.readFile(this.layoutPath, "utf8", layoutFileCallback);
					} else {
						layoutFileCallback(null, this.layout);
					}
				}
			}

			/**
    * Render compiled template with given set of data
    * @param {Object} data
    * @param {renderCallback} callback
    */

		}, {
			key: "render",
			value: function render(data, callback) {
				if (data.constructor != Object) {
					throw new Error("Invalid input data");
				}

				try {
					var _eHtml = function _eHtml(input) {
						// noinspection EqualityComparisonWithCoercionJS
						if (input == undefined) {
							return "";
						}

						// noinspection EqualityComparisonWithCoercionJS
						if (input.constructor === Date) {
							return input.toLocaleString(_locale, { timeZone: _timezone });
						}

						// noinspection EqualityComparisonWithCoercionJS
						return input.toLocaleString(_locale).replace(SPEC_CHAR_REGEX, function (char) {
							return "&#" + char.charCodeAt(0) + ";";
						});
					};

					var __out = function __out(input) {
						if (input == undefined) {
							return "";
						}

						return input.toLocaleString(_locale);
					};

					// Local variables which are used with compiled code in eval()
					//noinspection JSUnusedLocalSymbols
					var __data = data,
					    __output = "",
					    __defines = {},
					    __blocks = {},
					    __hlprs = Jumplate.Parser.helpers,
					    __bhlprs = Jumplate.Parser.blockHelpers,
					    __context = this.context,
					    _locale = this.locale || Jumplate.locale,
					    _timezone = this.timeZone || Jumplate.timeZone,
					    __loc = function (key) {
						return Jumplate.localizator(key, _locale);
					} || function (key) {
						return key;
					};

					eval(this.output);

					callback(null, __output);
				} catch (e) {
					callback(e, null);
				}
			}

			// noinspection JSUnusedGlobalSymbols
			/**
    * Set locale code which should be used for toLocaleString when printing variables
    * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
    */

		}, {
			key: "setLocale",
			value: function setLocale(localeCode) {
				// Commented out for performance reason. Developer should know if he set valid locale.
				// if (!/^[a-z]{2}-[A-Z]{2}$/.test(localeCode)) {
				// 	throw new Error("Invalid locale code.");
				// }

				this.locale = localeCode;
			}

			// noinspection JSUnusedGlobalSymbols
			/**
    * Set timezone which should be used for toLocaleString when printing Date
    * @param {string} timeZone
    */

		}, {
			key: "setTimeZone",
			value: function setTimeZone(timeZone) {
				this.timeZone = timeZone;
			}

			//</editor-fold>

		}], [{
			key: "fromCache",
			value: function fromCache(cachedTemplate) {
				var jumplate = new Jumplate();
				jumplate.output = cachedTemplate;

				return jumplate;
			}

			/**
    * Register function which will be called for each {loc} command; Localization key will be given as parameter
    * @param localizationHandler
    * @throws
    */

		}, {
			key: "registerLocalizator",
			value: function registerLocalizator(localizationHandler) {
				if (typeof localizationHandler !== "function") {
					throw new Error("Parameter must be function");
				}

				Jumplate.localizator = localizationHandler;
			}

			/**
    * Register helper
    * @param {String} name
    * @param {Function} helper
    * @throws {Error}
    */

		}, {
			key: "registerHelper",
			value: function registerHelper(name, helper) {
				// Check if helper's name is reserved Jumplate macro
				if (Jumplate.LexicalAnalyser.keyWords.indexOf(name) != -1) {
					throw new Error("You are registering helper '" + name + ")' but this name is reserved.");
				} else if (typeof helper != "function") {
					throw new Error("Your helper '" + name + "' is not a function.");
				} else if (Jumplate.Parser.blockHelpers.hasOwnProperty(name)) {
					throw new Error("Helper '" + name + " is already registered as block helper.");
				}

				Jumplate.Parser.helpers[name] = helper;
				Jumplate.LexicalAnalyser.registerHelper(name);
			}

			/**
    * Register block helper
    * @param {String} name
    * @param {Function} helper
    * @throws {Error}
    */

		}, {
			key: "registerBlockHelper",
			value: function registerBlockHelper(name, helper) {
				// Check if helper's name is reserved Jumplate macro
				if (Jumplate.LexicalAnalyser.keyWords.indexOf(name) != -1) {
					throw new Error("You are registering helper '" + name + ")' but this name is reserved.");
				} else if (typeof helper != "function") {
					throw new Error("Your helper '" + name + "' is not a function.");
				} else if (Jumplate.Parser.helpers.hasOwnProperty(name)) {
					throw new Error("Helper '" + name + " is already registered as non-block helper.");
				}

				Jumplate.Parser.blockHelpers[name] = helper;
				Jumplate.LexicalAnalyser.registerBlockHelper(name);
			}

			// noinspection JSUnusedGlobalSymbols
			/**
    * Set default locale code which should be used for toLocaleString; instance setLocale overrides this.
    * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
    */

		}, {
			key: "setLocale",
			value: function setLocale(localeCode) {
				// Commented out for performance reason. Developer should know if he set valid locale.
				// if (!/^[a-z]{2}-[A-Z]{2}$/.test(localeCode)) {
				// 	throw new Error("Invalid locale code.");
				// }

				Jumplate.locale = localeCode;
			}

			// noinspection JSUnusedGlobalSymbols
			/**
    * Set default timezone which should be used for toLocaleString; instance setTimeZone overrides this.
    * @param {string} timeZone
    */

		}, {
			key: "setTimeZone",
			value: function setTimeZone(timeZone) {
				Jumplate.timeZone = timeZone;
			}

			// noinspection JSUnusedGlobalSymbols
			/**
    * Escape HTML in given input
    * @param input
    * @returns {string | void | *}
    */

		}, {
			key: "escape",
			value: function escape(input) {
				if (input == undefined) {
					// match null too
					return "";
				}

				return input.replace(SPEC_CHAR_REGEX, function (char) {
					return "&#" + char.charCodeAt(0) + ";";
				});
			}
		}]);

		return Jumplate;
	}();

	if (isNode) {
		Jumplate.LexicalAnalyser = require("./LexicalAnalyser").LexicalAnalyser;
		Jumplate.Parser = require("./Parser").Parser;
	}

	exports.Jumplate = Jumplate;
})(typeof window !== "undefined" ? window : module.exports, typeof module !== "undefined" && module.exports !== undefined && typeof require === "function");

/************ LexicalAnalyser ************/
(function (exports) {
	/**
  * Definition of Virtual type Token
  * @typedef {{type: LexicalAnalyser.TokenTypes, value: String}} Token
  */

	// Private variables for saving often accessed data
	var _startingSymbolSearchRegExp, _startingSymbolRegExp, TokenTypes, _startingSymbol, _terminalMapArray, _endingSymbolRegExp;
	var _startToEndKeyWordMap = new Map();

	var _keyWords = ["defined", "define", "block", "include", "var", "for", "in", "of", "elseif", "if", "else", // elseif must be before else and if
	"first", "last", "even", "odd", "loc", "debug"];

	var _keyWordsEndings = ["/define", "/defined", "/block", "/for", "/if", "/first", "/last", "/even", "/odd"];

	var symbolForEscape = ["{", "}", ".", "$", "^", "(", ")"];
	function escapeSymbol(symbol) {
		var out = "";
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = symbol[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var l = _step.value;

				if (symbolForEscape.indexOf(l) != -1) {
					out += "\\" + l;
				} else {
					out += l;
				}
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		return out;
	}

	/**
  * This class do lexical analyse over template code
  */

	var LexicalAnalyser = function () {
		_createClass(LexicalAnalyser, null, [{
			key: "KeyWords",


			//<editor-fold desc="Enums">

			/**
    *
    * @returns {{Defined: string, Define: string, Block: string, Include: string, Var: string, For: string, If: string, ElseIf: string, Else: string, Loc: string, First: string, Last: string, Even: string, Odd: string, Debug: string}}
    * @enum
    */
			get: function get() {
				//noinspection JSValidateTypes
				return {
					Defined: "defined",
					Define: "define",
					Block: "block",
					Include: "include",
					Var: "var",
					For: "for",
					If: "if",
					ElseIf: "elseif",
					Else: "else",
					Loc: "loc",
					First: "first",
					Last: "last",
					Even: "even",
					Odd: "odd",
					Debug: "debug"
				};
			}

			/**
    * @returns {{EndDefined: string, EndDefine: string, EndBlock: string, EndFor: string, EndIf: string, EndFirst: string, EndLast: string, EndEven: string, EndOdd: string}}
    * @@enum
    */

		}, {
			key: "EndKeyWords",
			get: function get() {
				return {
					EndDefined: "/defined",
					EndDefine: "/define",
					EndBlock: "/block",
					EndFor: "/for",
					EndIf: "/if",
					EndFirst: "/first",
					EndLast: "/last",
					EndEven: "/even",
					EndOdd: "/odd"
				};
			}

			/**
    * @enum
    */

		}, {
			key: "TokenTypes",
			get: function get() {
				//noinspection JSValidateTypes
				return {
					/** @type {LexicalAnalyser.TokenTypes} */
					HtmlContext: "HtmlContext",
					/** @type {LexicalAnalyser.TokenTypes} */
					CommandStartingBracket: "CommandStartingBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					CommandEndingBracket: "CommandEndingBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					Variable: "Variable",
					/** @type {LexicalAnalyser.TokenTypes} */
					KeyWord: "KeyWord",
					/** @type {LexicalAnalyser.TokenTypes} */
					KeyWordEnding: "KeyWordEnding",
					/** @type {LexicalAnalyser.TokenTypes} */
					String: "String",
					/** @type {LexicalAnalyser.TokenTypes} */
					Number: "Number",
					/** @type {LexicalAnalyser.TokenTypes} */
					Boolean: "Boolean",
					/** @type {LexicalAnalyser.TokenTypes} */
					Object: "Object",
					/** @type {LexicalAnalyser.TokenTypes} */
					ArrayLeftBracket: "ArrayLeftBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					ArrayRightBracket: "ArrayRightBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					Comma: "Comma",
					/** @type {LexicalAnalyser.TokenTypes} */
					Colon: "Colon",
					/** @type {LexicalAnalyser.TokenTypes} */
					Semicolon: "Semicolon",
					/** @type {LexicalAnalyser.TokenTypes} */
					Dot: "Dot",
					/** @type {LexicalAnalyser.TokenTypes} */
					WhiteSpace: "WhiteSpace",
					/** @type {LexicalAnalyser.TokenTypes} */
					Comment: "Comment",
					/** @type {LexicalAnalyser.TokenTypes} */
					Assignment: "Assignment",
					/** @type {LexicalAnalyser.TokenTypes} */
					ArithmeticOperator: "ArithmeticOperator",
					/** @type {LexicalAnalyser.TokenTypes} */
					Comparison: "Comparison",
					/** @type {LexicalAnalyser.TokenTypes} */
					Identifier: "Identifier",
					/** @type {LexicalAnalyser.TokenTypes} */
					EndOfFile: "EndOfFile",
					/** @type {LexicalAnalyser.TokenTypes} */
					ClassIdentifier: "ClassIdentifier",
					/** @type {LexicalAnalyser.TokenTypes} */
					LeftBracket: "LeftFunctionBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					RightBracket: "RightBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					FunctionCall: "FunctionCall",
					/** @type {LexicalAnalyser.TokenTypes} */
					Negation: "Negation",
					/** @type {LexicalAnalyser.TokenTypes} */
					ObjectStartingBracket: "ObjectStartingBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					ObjectEndingBracket: "ObjectEndingBracket",
					/** @type {LexicalAnalyser.TokenTypes} */
					LogicalOperator: "LogicalOperator"
				};
			}

			//</editor-fold>

			//<editor-fold desc="Static Properties">

			/**
    * Symbol used for command start
    * @return {string}
    */

		}, {
			key: "startingSymbol",
			get: function get() {
				return "{{";
			}

			/**
    * Symbol used for command end
    * @return {string}
    */

		}, {
			key: "endingSymbol",
			get: function get() {
				return "}}";
			}

			/**
    * RegExp for starting symbol
    * Symbol "{", not foregoing by escape
    * @private
    * @type {RegExp}
    */

		}, {
			key: "startingSymbolSearchRegExp",
			get: function get() {
				if (!_startingSymbolSearchRegExp) {
					_startingSymbolSearchRegExp = new RegExp("(?:[^\\\\]|^)(" + escapeSymbol(LexicalAnalyser.startingSymbol) + ")");
				}
				return _startingSymbolSearchRegExp;
				// ! String.search() match not-matching group !
			}

			/**
    * RegExp for starting symbol
    * @private
    * @type {RegExp}
    */

		}, {
			key: "startingSymbolRegExp",
			get: function get() {
				if (!_startingSymbolRegExp) {
					_startingSymbolRegExp = new RegExp("^" + escapeSymbol(LexicalAnalyser.startingSymbol));
				}
				return _startingSymbolRegExp;
			}

			/**
    * RegExp for ending symbol
    * @private
    * @type {RegExp}
    */

		}, {
			key: "endingSymbolRegExp",
			get: function get() {
				if (!_endingSymbolRegExp) {
					_endingSymbolRegExp = new RegExp("^" + escapeSymbol(LexicalAnalyser.endingSymbol));
				}
				return _endingSymbolRegExp;
			}

			/**
    * List of keywords
    * @return {string[]}
    */

		}, {
			key: "keyWords",
			get: function get() {
				return _keyWords;
			}

			/**
    * List of ending keywords
    * @return {string[]}
    */

		}, {
			key: "keyWordsEndings",
			get: function get() {
				return _keyWordsEndings;
			}

			/**
    * Define Terminals and their regular expressions for matching
    * @private
    */

		}, {
			key: "terminals",
			get: function get() {
				if (!_terminalMapArray) {

					// Sort by keyword length desc
					//
					var orderByLengthDesc = function orderByLengthDesc(a, b) {
						if (a.length > b.length) return -1;
						if (a.length < b.length) return 1;
						return 0;
					};

					// Ending keywords


					var map = [];

					// Commet
					map.push({ terminal: TokenTypes.Comment, regex: new RegExp("^(\\*[\\s\\S]*?\\*)") });var keyWordsTerminal = "^(";
					var endKeywords = LexicalAnalyser.keyWordsEndings;
					endKeywords.sort(orderByLengthDesc);
					var il = endKeywords.length;
					for (var i = 0; i < il; i++) {
						keyWordsTerminal += "(?:" + endKeywords[i] + ")";

						if (i != il - 1) {
							keyWordsTerminal += "|";
						}
					}
					keyWordsTerminal += ")" + LexicalAnalyser.endingSymbol;
					map.push({ terminal: TokenTypes.KeyWordEnding, regex: new RegExp(keyWordsTerminal) });

					// Keywords
					keyWordsTerminal = "^(";
					var keywords = LexicalAnalyser.keyWords;
					keywords.sort(orderByLengthDesc);
					il = keywords.length;
					for (var _i = 0; _i < il; _i++) {
						keyWordsTerminal += "(?:" + keywords[_i] + ")";

						if (_i != il - 1) {
							keyWordsTerminal += "|";
						}
					}
					keyWordsTerminal += ")";
					map.push({ terminal: TokenTypes.KeyWord, regex: new RegExp(keyWordsTerminal) });

					map.push({ regex: new RegExp("^ [A-Z][a-zA-Z]*"), terminal: TokenTypes.ClassIdentifier }); // Před whitespace
					map.push({ regex: new RegExp("^\\s+"), terminal: TokenTypes.WhiteSpace });
					map.push({
						regex: new RegExp("^\\$[a-z][a-zA-Z0-9]*(?:(?:\\[\"(?:(?:[^\"\\\\])*|(?:\\\\\"?)*)*\"\\])|(?:\\.[a-zA-Z][a-zA-Z0-9]*))*"),
						terminal: TokenTypes.Variable
					});
					// map.push({regex: new RegExp("^\\.[a-z][a-zA-Z0-9]*\\(((,)?)*\\)"), terminal:  TokenTypes.FunctionCall});
					map.push({ regex: _startingSymbolRegExp, terminal: TokenTypes.CommandStartingBracket });
					map.push({ regex: _endingSymbolRegExp, terminal: TokenTypes.CommandEndingBracket });
					map.push({ regex: new RegExp("^\\{"), terminal: TokenTypes.ObjectStartingBracket });
					map.push({ regex: new RegExp("^\\}"), terminal: TokenTypes.ObjectEndingBracket });
					map.push({ regex: new RegExp("^\"(?:(?:[^\"\\\\])*|(?:\\\\\"?)*)*\""), terminal: TokenTypes.String });
					map.push({ regex: new RegExp("^(?:\\+|-)?[0-9]+(?:\\.[0-9]+)?"), terminal: TokenTypes.Number });
					map.push({ regex: new RegExp("^(?:(?:true)|(?:false))"), terminal: TokenTypes.Boolean });
					map.push({ regex: new RegExp("^="), terminal: TokenTypes.Assignment });
					map.push({ regex: new RegExp("^;"), terminal: TokenTypes.Semicolon });
					map.push({ regex: new RegExp("^,"), terminal: TokenTypes.Comma });
					map.push({ regex: new RegExp("^:"), terminal: TokenTypes.Colon });
					map.push({
						regex: new RegExp("^((?:&)|(?:\\|)|(?:\\^))"),
						terminal: TokenTypes.LogicalOperator
					});
					map.push({
						regex: new RegExp("^((?:\\+\\+)|(?:--)|(?:\\+)|(?:-)|(?:\\*)|(?:/)|(?:%))"),
						terminal: TokenTypes.ArithmeticOperator
					});
					map.push({
						regex: new RegExp("^((?:>)|(?:<)|(?:==)|(?:===)|(?:!=)|(?:!==)|(?:>=)|(?:<=))"),
						terminal: TokenTypes.Comparison
					});

					// Identifier must be after keywords
					map.push({ regex: new RegExp("^[a-z][a-zA-Z]*"), terminal: TokenTypes.Identifier });

					// Last
					map.push({ regex: new RegExp("^\\("), terminal: TokenTypes.LeftBracket });
					map.push({ regex: new RegExp("^\\)"), terminal: TokenTypes.RightBracket });
					map.push({ regex: new RegExp("^\\."), terminal: TokenTypes.Dot });
					map.push({ regex: new RegExp("^!"), terminal: TokenTypes.Negation });
					map.push({ regex: new RegExp("^\\["), terminal: TokenTypes.ArrayLeftBracket });
					map.push({ regex: new RegExp("^\\]"), terminal: TokenTypes.ArrayRightBracket });

					_terminalMapArray = map;
				}

				return _terminalMapArray;
			}

			//</editor-fold>

			//<editor-fold desc="Constructor">

			/**
    * Constructor
    */

		}]);

		function LexicalAnalyser() {
			_classCallCheck(this, LexicalAnalyser);

			/**
    * Hold last toen
    * @type {Token|null}
    */
			this.lastToken = null;
		}

		//</editor-fold>

		//<editor-fold desc="Static Methods">

		/**
   * Register helper to lexer
   * @param {String} name
   */


		_createClass(LexicalAnalyser, [{
			key: "getToken",


			//</editor-fold>

			//<editor-fold desc="Public Methods">

			/**
    * Return Token which is first in input
    * @param {string} input Template code
    * @param {number} inputOffset Offset Offset of already processed code in input
    * @param {boolean} [peek] Mark as peek call - will not change holded last token
    * @returns {[Error, Token, Number]}
    */
			value: function getToken(input, inputOffset, peek) {
				if (typeof input != "string") {
					return [new Error("Invalid argument input. No input given."), null, inputOffset];
				}

				if (inputOffset >= input.length) {
					return [null, {
						type: TokenTypes.EndOfFile,
						value: null
					}, inputOffset];
				}

				var index;
				var lastToken = this.lastToken;

				if (lastToken == null || lastToken.type == TokenTypes.CommandEndingBracket || lastToken.type == TokenTypes.HtmlContext) {
					input = input.slice(inputOffset);
					index = input.search(_startingSymbolSearchRegExp);

					var indexFixed = false;

					// Pokud byl nalezen startovací symbol na začátku (index == 0), ale nejedná se o první symbol
					// +1 je fix chyby string.search(), která nebere v potaz not-matching group
					if (index == 0 && input[0] != _startingSymbol[0]) {
						index++;
						indexFixed = true;
					}

					if (index != 0) {
						var substring = index == -1 ? input : input.slice(0, index + !indexFixed);
						lastToken = {
							type: TokenTypes.HtmlContext,
							value: substring
						};

						if (!peek) this.lastToken = lastToken;

						return [null, lastToken, inputOffset + substring.length];
					} else {
						lastToken = {
							type: TokenTypes.CommandStartingBracket,
							value: _startingSymbol
						};

						if (!peek) this.lastToken = lastToken;

						return [null, lastToken, inputOffset + _startingSymbol.length];
					}
				} else {
					var error, token, offset;

					var _LexicalAnalyser$sear = LexicalAnalyser.searchForToken(input, inputOffset);

					var _LexicalAnalyser$sear2 = _slicedToArray(_LexicalAnalyser$sear, 3);

					error = _LexicalAnalyser$sear2[0];
					token = _LexicalAnalyser$sear2[1];
					offset = _LexicalAnalyser$sear2[2];


					if (error) {
						return [error, null, offset];
					}

					if (!peek) this.lastToken = token;

					if (token.type == TokenTypes.WhiteSpace) {
						var _LexicalAnalyser$sear3 = LexicalAnalyser.searchForToken(input, offset);

						var _LexicalAnalyser$sear4 = _slicedToArray(_LexicalAnalyser$sear3, 3);

						error = _LexicalAnalyser$sear4[0];
						token = _LexicalAnalyser$sear4[1];
						offset = _LexicalAnalyser$sear4[2];
					}

					return [error, token, offset];
				}
			}

			/**
    * Just for debug - print list of all found tokens to console
    * @private
    */

		}, {
			key: "doCompleteLexicalAnalyse",
			value: function doCompleteLexicalAnalyse(input) {
				var err,
				    token,
				    offset = 0;

				var _getToken = this.getToken(input, offset);

				var _getToken2 = _slicedToArray(_getToken, 3);

				err = _getToken2[0];
				token = _getToken2[1];
				offset = _getToken2[2];


				while (token.type != TokenTypes.EndOfFile) {
					var _getToken3 = this.getToken(input, offset);

					var _getToken4 = _slicedToArray(_getToken3, 3);

					err = _getToken4[0];
					token = _getToken4[1];
					offset = _getToken4[2];
				}

				// Reinicializace - vrácení instance do původního stavu
				this.lastToken = null;
			}

			//</editor-fold>

		}], [{
			key: "registerHelper",
			value: function registerHelper(name) {
				_keyWords.push(name);
			}

			/**
    * Register block helper to lexer
    * @param {String} name
    */

		}, {
			key: "registerBlockHelper",
			value: function registerBlockHelper(name) {
				_keyWords.push(name);
				_keyWordsEndings.push("/" + name);
				// Map keyword startto its end
				_startToEndKeyWordMap.set(name, "/" + name);
			}

			/**
    * Return ending keyword for given keyword
    * @param {LexicalAnalyser.KeyWords} start Keyword identifying block
    * @returns {LexicalAnalyser.EndKeyWords}
    */

		}, {
			key: "getEndKeyWordByStart",
			value: function getEndKeyWordByStart(start) {
				return _startToEndKeyWordMap.get(start);
			}

			/**
    * Return line for given symbol position
    * @param {Number} pos
    * @param {string} input
    * @return {Number}
    */

		}, {
			key: "getLineNumberOfSymbolPosition",
			value: function getLineNumberOfSymbolPosition(pos, input) {
				return input.substr(0, pos).split(/\r?\n/).length;
			}

			/**
    * Return position of symbol in its line
    * @param {Number} pos
    * @param {string} input
    * @return {Number}
    */

		}, {
			key: "getPositionOfSymbolInLine",
			value: function getPositionOfSymbolInLine(pos, input) {
				var lastPos = 0;

				// Nalezení pozic všech \r\n zneužitím rozšířeného replace
				input.slice(0, pos).replace(/\r?\n/g, function (match, index) {
					lastPos = index + match.length;
				});

				return pos - lastPos;
			}

			/**
    * Search first token in the input
    * @private
    * @param input
    * @param inputOffset
    * @returns {[ Error, Token, Number ]}
    */

		}, {
			key: "searchForToken",
			value: function searchForToken(input, inputOffset) {
				var match,
				    matchFound,
				    terminal = null;
				var inputSubstr = input.slice(inputOffset);
				var offset = inputOffset;

				// Test each terminal
				var terminals = LexicalAnalyser.terminals;
				for (var i = 0; i < terminals.length; i++) {
					match = inputSubstr.match(terminals[i].regex);
					if (match != null) {
						terminal = terminals[i].terminal;
						matchFound = true;
						break;
					}
				}

				// Error - unexpected symbol
				if (!matchFound) {
					return [new Error("Unexpected '" + inputSubstr.slice(0, 20) + "' found at line " + LexicalAnalyser.getLineNumberOfSymbolPosition(inputOffset, input)), null, null];
				}

				// If match-group exists use it, or use default match
				match = match[1] || match[0];
				offset += match.length;

				return [null, {
					type: terminal,
					value: match
				}, offset];
			}
		}]);

		return LexicalAnalyser;
	}();

	TokenTypes = LexicalAnalyser.TokenTypes;
	_startingSymbolSearchRegExp = LexicalAnalyser.startingSymbolSearchRegExp;
	_startingSymbolRegExp = LexicalAnalyser.startingSymbolRegExp;
	_startingSymbol = LexicalAnalyser.startingSymbol;
	_endingSymbolRegExp = LexicalAnalyser.endingSymbolRegExp;

	// Create map of keywords paired to their ends
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Block, LexicalAnalyser.EndKeyWords.EndBlock);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.If, LexicalAnalyser.EndKeyWords.EndIf);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Define, LexicalAnalyser.EndKeyWords.EndDefine);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Defined, LexicalAnalyser.EndKeyWords.EndDefined);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.For, LexicalAnalyser.EndKeyWords.EndFor);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.First, LexicalAnalyser.EndKeyWords.EndFirst);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Last, LexicalAnalyser.EndKeyWords.EndLast);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Even, LexicalAnalyser.EndKeyWords.EndEven);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Odd, LexicalAnalyser.EndKeyWords.EndOdd);

	exports.LexicalAnalyser = LexicalAnalyser;
})(typeof window !== "undefined" ? window.Jumplate : module.exports);

/************ Parser ************/
(function (exports, isNode) {
	var $fs, $path, LexicalAnalyser;

	if (isNode) {
		$fs = require("fs");
		$path = require("path");
	}

	LexicalAnalyser = isNode ? require("./LexicalAnalyser").LexicalAnalyser : exports.LexicalAnalyser;

	var TokenTypes = LexicalAnalyser.TokenTypes;
	var KeyWords = LexicalAnalyser.KeyWords;

	// Name of variable which is used to access key of iterated object in for in(/of)
	var iteratorName = "itemKey";

	/**
  * Parser, class which do main part of compilation; it checks syntax too
  */

	var Parser = function () {
		//<editor-fold dest="Constructor">

		/**
   * @param {string} templateContent Template code
   * @param {string} templateBaseDirectory Base directory for template, used for file including
   * @param {string} [fileName] emplate file name - used in errors
   */
		function Parser(templateContent, templateBaseDirectory, fileName) {
			_classCallCheck(this, Parser);

			/**
    * @private
    * @type {LexicalAnalyser}
    */
			this.lexer = new LexicalAnalyser();

			/**
    * Current token
    * @private
    * @type {Token|null}
    */
			this.token = null;

			/**
    * Offset obdržený z lexeru - offset, který se má nastavit, po přijetí současného tokenu
    * @private
    * @type {number}
    */
			this.tokenOffset = 0;

			/**
    * Vstupní kód šablony
    * @private
    */
			this.templateContent = templateContent;

			/**
    * Offset od začátku kódu šablony
    * @private
    * @type {number}
    */
			this.templateContentOffset = 0;

			/**
    * Základní složka template pro řešení závislostí na souborech
    * @private
    * @type {string}
    */
			this.templateBaseDirectory = templateBaseDirectory;

			/**
    * Název souboru (šablony) pro upřesnění chybových hlášení
    * @private
    * @type {string | null}
    */
			this.fileName = fileName || null;

			/**
    * Count created cycles and provide unique identification of cycles when more cycles inside another
    * @type {number}
    */
			this.iteratorCounter = 0;

			// debug
			// this.lexer.doCompleteLexicalAnalyse(templateContent);
		}

		//</editor-fold>

		//<editor-fold dest="Virtual Callbacks">

		/**
   * Virtual callback for method parse()
   * @callback parseCallback
   * @param {Error} error Chyba
   * @param {string | null} compiledTemplate Zkompilovaná šablona
   */

		/**
   * Virtual callback for method processBlock()
   * @callback partialContentCallback
   * @param {Error} error Chyba
   * @param {string | null} partialContent Část zkompilovného kódu
   */

		//</editor-fold>

		//<editor-fold dest="Public Methods">

		/**
   * Compile input and return compiled result
   * @param {parseCallback} callback
   */


		_createClass(Parser, [{
			key: "parse",
			value: function parse(callback) {
				this.processBlock(null, function (err, output) {
					callback(err, err ? null : output);
				});
			}

			//</editor-fold>

			//<editor-fold dest="Private Methods">

			/**
    * Create error with given message
    * @private
    * @param {string} message
    * @returns {Error}
    */

		}, {
			key: "createError",
			value: function createError(message) {
				return new Error(message + " Unexpected token of type '" + this.token.type + "' with value '" + this.token.value + "' found" + (this.fileName ? " in " + $path.join(this.templateBaseDirectory, this.fileName) : "") + " at line " + LexicalAnalyser.getLineNumberOfSymbolPosition(this.templateContentOffset, this.templateContent) + ":" + LexicalAnalyser.getPositionOfSymbolInLine(this.templateContentOffset, this.templateContent) + ".");
			}

			//noinspection JSMethodCanBeStatic
			/**
    * Compile/translate variable name
    * @private
    * @param {string} variable
    * @returns {string}
    */

		}, {
			key: "translateVariableName",
			value: function translateVariableName(variable) {
				variable = variable.slice(1);

				// If it's $itemKey the iteration counter
				if (variable === iteratorName) {
					return '__itemKey';
				} else {
					var dotPos = variable.indexOf(".");
					var bracketPos = variable.indexOf("[");

					if (dotPos === -1 && bracketPos === -1) {
						return '__data["' + variable + '"]';
					}

					var baseEnd = dotPos !== -1 && (dotPos < bracketPos || bracketPos === -1) ? dotPos : bracketPos;

					return '__data["' + variable.slice(0, baseEnd) + '"]' + variable.slice(baseEnd);
				}
			}

			/**
    * Helping procedure for getting token from input through lexical analyser
    * @private
    * @throws {Error}
    */

		}, {
			key: "readToken",
			value: function readToken() {
				// Pokud je offset vstupu roven offsetu tokenu,
				// tak je čtecí hlava připravena ke čtení dalšího tokenu,
				// jinak přerušíme - aby nedocházelo ke zbytečnému znovu-zpracování
				if (this.token != null && this.tokenOffset !== this.templateContentOffset) {
					return;
				}

				var err, token, offset;

				var _lexer$getToken = this.lexer.getToken(this.templateContent, this.templateContentOffset);

				var _lexer$getToken2 = _slicedToArray(_lexer$getToken, 3);

				err = _lexer$getToken2[0];
				token = _lexer$getToken2[1];
				offset = _lexer$getToken2[2];


				if (err) {
					err.message += this.fileName ? " in " + $path.join(this.templateBaseDirectory, this.fileName) : "";
					throw err;
				}

				this.token = token;
				this.tokenOffset = offset;
			}

			/**
    * Eat (remove) token from input and read new
    * @private
    * @param {LexicalAnalyser.TokenTypes} tokenType Expecting token type
    * @throws {Error}
    */

		}, {
			key: "eat",
			value: function eat(tokenType) {
				if (this.token.type === tokenType) {
					// Aktualizace offsetu - přesunutí kurzoru za poslední token
					this.templateContentOffset = this.tokenOffset;
					this.readToken();
					return;
				}

				throw this.createError("Token of type " + tokenType + " expected.");
			}

			/**
    * Look for next token without moving input
    * @private
    * @returns {Token}
    * @throws {Error}
    */

		}, {
			key: "peek",
			value: function peek() {
				this.readToken();

				var err, token;

				var _lexer$getToken3 = this.lexer.getToken(this.templateContent, this.tokenOffset, true /*, this.token*/);

				var _lexer$getToken4 = _slicedToArray(_lexer$getToken3, 2);

				err = _lexer$getToken4[0];
				token = _lexer$getToken4[1];


				if (err) {
					throw err;
				}

				return token;
			}

			/**
    * Zpracuje blok příkazů; Tvoří hlavní rekurzivní cyklus pro zpracování šablony.
    * @private
    * @param {LexicalAnalyser.KeyWords | null} parent Identifikátor rodičovského bloku
    * @param {partialContentCallback} callback
    */

		}, {
			key: "processBlock",
			value: function processBlock(parent, callback) {
				var _this2 = this;

				// Proměnná obsahující výstup bloku
				var output = "";

				try {
					this.readToken();

					// HTML Context
					if (this.token.type === TokenTypes.HtmlContext) {
						if (this.token.value.trim().length > 0) {
							output += '__output+="' + this.token.value. /*replace(/(")|((?:\r|\n|(?:\r\n))+)|(\\\{)|(\\)/g, function(_, a,b,c,d) {
                                                   if (a) return '\\"';
                                                   else if (b) return "\\n";
                                                   else if (c) return "{";
                                                   else return "\\\\";
                                                   })*/replace(/\\\{\{/g, "{{").replace(/\\/g, "\\\\").replace(/(\r\n)|(\r|\n)+/g, "\\n").replace(/"/g, '\\"') + '";';
						}
						this.eat(TokenTypes.HtmlContext);
					}
					// CMD
					else if (this.token.type === TokenTypes.CommandStartingBracket) {
							this.eat(TokenTypes.CommandStartingBracket);

							// Print variable (escaped)
							if (this.token.type === TokenTypes.Variable) {
								output += '__output+=_eHtml(' + this.translateVariableName(this.token.value) + ');';
								this.eat(TokenTypes.Variable);
								this.eat(TokenTypes.CommandEndingBracket);
							}
							// Unescaped variable print
							else if (this.token.type === TokenTypes.Negation) {
									this.eat(TokenTypes.Negation);
									var varName = this.token.value;
									this.eat(TokenTypes.Variable);
									output += '__output+=__out(' + this.translateVariableName(varName) + ');';
									this.eat(TokenTypes.CommandEndingBracket);
								}
								// Comment
								else if (this.token.type === TokenTypes.Comment) {
										this.eat(TokenTypes.Comment);
										this.eat(TokenTypes.CommandEndingBracket);
									} else {
										var keyword = this.token.value;

										this.eat(TokenTypes.KeyWord);

										// Callback for block cmds
										var blockCmdCallback = function blockCmdCallback(err, contentOutput) {
											if (err) {
												callback(err, null);
												return;
											}

											output += contentOutput;
											_this2.finalizeMacro(parent, callback, output);
										};

										switch (keyword) {
											case KeyWords.For:
												this.cmdFor(blockCmdCallback);
												return;
											case KeyWords.If:
												this.cmdIf(blockCmdCallback);
												return;
											case KeyWords.ElseIf:
												var cond = this.processExpression(TokenTypes.CommandEndingBracket);
												this.eat(TokenTypes.CommandEndingBracket);
												output += '}else if(' + cond + '){';
												break;
											case KeyWords.Else:
												output += '}else{';
												this.eat(TokenTypes.CommandEndingBracket);
												break;
											case KeyWords.First:
												this.cmdFirst(blockCmdCallback);
												return;
											case KeyWords.Last:
												this.cmdLast(blockCmdCallback);
												return;
											case KeyWords.Even:
												this.cmdEven(blockCmdCallback);
												return;
											case KeyWords.Odd:
												this.cmdOdd(blockCmdCallback);
												return;
											case KeyWords.Loc:
												output += this.cmdLoc();
												break;
											case KeyWords.Block:
												this.cmdBlock(blockCmdCallback);
												return;
											case KeyWords.Include:
												this.cmdInclude(blockCmdCallback);
												return;
											case KeyWords.Defined:
												this.cmdDefined(blockCmdCallback);
												return;
											case KeyWords.Define:
												this.cmdDefine(blockCmdCallback);
												return;
											case KeyWords.Var:
												output += this.cmdVar();
												break;
											case KeyWords.Debug:
												output += this.cmdDebug();
												break;
											default:
												// Validate if helper exists
												if (!(keyword in Parser.helpers) && !(keyword in Parser.blockHelpers)) {
													callback(new Error("Unsupported command '" + keyword + "' found at line " + LexicalAnalyser.getLineNumberOfSymbolPosition(this.templateContentOffset, this.templateContent) + (this.fileName ? " in " + $path.join(this.templateBaseDirectory, this.fileName) : "") + "."), null);

													return;
												}

												if (keyword in Parser.helpers) {
													// Process helper
													output += this.helperCmd(keyword);
												} else {
													// Process block helper
													this.blockHelperCmd(keyword, blockCmdCallback);
													return;
												}
										}
									}
						}

					this.finalizeMacro(parent, callback, output);
				} catch (err) {
					callback(err, null);
				}
			}

			/**
    * Finallize block macro
    * @param parent
    * @param callback
    * @param output
    */

		}, {
			key: "finalizeMacro",
			value: function finalizeMacro(parent, callback, output) {
				try {
					this.readToken();

					// EOF - výstup z rekurze - vrácení finálního překladu
					if (parent == null && this.token.type === TokenTypes.EndOfFile) {
						this.eat(TokenTypes.EndOfFile);
						callback(null, output);
					} else {
						var nextToken = this.peek();

						// Pokud se jedná o konečný příkaz (existuje parent), který ukončuje současný kontext
						//noinspection JSValidateTypes
						if (parent != null && this.token.type === TokenTypes.CommandStartingBracket && nextToken.type === TokenTypes.KeyWordEnding && LexicalAnalyser.getEndKeyWordByStart(parent) === nextToken.value) {

							this.eat(TokenTypes.CommandStartingBracket);
							this.eat(TokenTypes.KeyWordEnding);
							this.eat(TokenTypes.CommandEndingBracket);

							callback(null, output);
						}
						// Parent block still continue
						else {
								this.processBlock(parent, function (err, nextOutput) {
									callback(err, output + nextOutput);
								});
							}
					}
				} catch (err) {
					callback(err, null);
				}
			}

			/**
    * Parse expression
    * @private
    * @param {LexicalAnalyser.TokenTypes} endToken
    * @returns {string} output
    * @throws {Error}
    */

		}, {
			key: "processExpression",
			value: function processExpression(endToken) {
				// TODO: rework reading expression - integrate to lexer - read from curent pos to indexOf endToken

				var output = "";
				while (this.token.type !== endToken && this.token.type !== TokenTypes.EndOfFile) {
					// console.log(this.token);
					if (this.token.type === TokenTypes.Variable) {
						output += this.translateVariableName(this.token.value);
					} else {
						output += this.token.value;
					}

					this.eat(this.token.type);
				}

				return output;
			}

			/**
    * Naparsuje inkrementační výraz - změnu hodnoty proměnné v určitém kroku (eg. ++, +=, /=, etc..)
    * @private
    * @returns {string} output
    * @throws {Error}
    */

		}, {
			key: "processValuation",
			value: function processValuation() {
				var variable = this.token.value;

				this.eat(TokenTypes.Variable);

				variable = this.translateVariableName(variable);
				var operator = this.token.value;

				this.eat(TokenTypes.ArithmeticOperator);

				if (operator === "++" || operator === "--") {
					return variable + operator;
				}
				// += | -= | /= | *= | %=
				else if (this.token.type === TokenTypes.Assignment) {
						this.eat(TokenTypes.Assignment);
						var value;

						if (this.token.type === TokenTypes.Number) {
							value = this.token.value;
							this.eat(TokenTypes.Number);
						} else if (this.token.type === TokenTypes.Variable) {
							value = this.translateVariableName(this.token.value);
							this.eat(TokenTypes.Variable);
						} else {
							throw this.createError("Final expression can contain only number or variable.");
						}

						return variable + operator + value;
					}
			}

			/**
    * Ověří, jestli je požadovaný blok prázdný, zpracuje jej, a vrátí true
    * Pokud se nejedná o prázdný blok, nic neprovádí a vrací false
    * @private
    * @param {LexicalAnalyser.KeyWords} parent
    * @returns {boolean}
    * @throws {Error}
    */

		}, {
			key: "tryToProcessEmptyBlock",
			value: function tryToProcessEmptyBlock(parent) {
				var nextToken = this.peek();

				if (parent != null && this.token.type === TokenTypes.CommandStartingBracket && nextToken.type === TokenTypes.KeyWordEnding && LexicalAnalyser.getEndKeyWordByStart(parent) === nextToken.value) {

					this.eat(TokenTypes.CommandStartingBracket);
					this.eat(TokenTypes.KeyWordEnding);
					this.eat(TokenTypes.CommandEndingBracket);

					return true;
				}

				return false;
			}

			//<editor-fold desc="Command's implementation">

			/**
    * Process user's custom helper command
    * @param {String} helper
    */

		}, {
			key: "helperCmd",
			value: function helperCmd(helper) {
				this.eat(TokenTypes.LeftBracket);
				var expr = this.processExpression(TokenTypes.RightBracket);
				this.eat(TokenTypes.RightBracket);
				this.eat(TokenTypes.CommandEndingBracket);

				return '__output+=__hlprs["' + helper + '"].call(__context,' + expr + ');';
			}

			/**
    * Process user's custom block helper command
    * @param {String} helper Helper's name / keyword
    * @param callback
    */

		}, {
			key: "blockHelperCmd",
			value: function blockHelperCmd(helper, callback) {
				try {
					if (this.token.type === TokenTypes.LeftBracket) {
						this.eat(TokenTypes.LeftBracket);
						var expr = this.processExpression(TokenTypes.RightBracket);
						this.eat(TokenTypes.RightBracket);
					}

					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(helper)) {
						callback(null, "");
						return;
					}

					this.processBlock(helper, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						// add block content
						var output = '__output+=__bhlprs["' + helper + '"].call(__context, (function(){var __output="";' + content + 'return __output;})(), ' + expr + ');';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd DEFINE - store code for later usage, not print out resul in place
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdDefine",
			value: function cmdDefine(callback) {
				try {
					var id = this.token.value;

					this.eat(TokenTypes.Identifier);
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.Define)) {
						// Empty block should exists too
						callback(null, '__blocks["' + id + '"]="";');
						return;
					}

					this.processBlock(KeyWords.Define, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = '__blocks["' + id + '"]=(function(){var __output="";' + content + 'return __output;})();';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd DEFINED - Ověření existence definice; v případě že existuje, vypíše se obsah bloku
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdDefined",
			value: function cmdDefined(callback) {
				try {
					var id = this.token.value;

					this.eat(TokenTypes.Identifier);
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.Defined)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.Defined, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						// if (definitions exists) add content to output
						var output = 'if(__blocks["' + id + '"]){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd BLOCK - uložení kódu pro pozdější použítí + v místě definice se nevypisuje
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdBlock",
			value: function cmdBlock(callback) {
				try {
					var id = this.token.value;

					this.eat(TokenTypes.Identifier);
					this.eat(TokenTypes.CommandEndingBracket);

					if (this.tryToProcessEmptyBlock(KeyWords.Block)) {
						// Empty block should exists too
						callback(null, '__blocks["' + id + '"]="";');
						return;
					}

					this.processBlock(KeyWords.Block, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = '__output+=__blocks["' + id + '"]=(function(){var __output="";' + content + 'return __output;})();';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd INCLUDE - vypíše obsah bloku (pokud přijímá identifikátor) nebo načte soubor (pokud přijímá řetězec)
    * @private
    * @param {partialContentCallback} callback
    * @returns {string} output
    */

		}, {
			key: "cmdInclude",
			value: function cmdInclude(callback) {
				try {
					if (this.token.type === TokenTypes.Identifier) {
						var id = this.token.value;

						this.eat(TokenTypes.Identifier);
						this.eat(TokenTypes.CommandEndingBracket);

						callback(null, '__output+=(__blocks["' + id + '"] || "");');
					} else if (this.token.type === TokenTypes.String) {
						if (!isNode) throw new Error("Importing file is impossible on client!");

						var path = this.token.value;

						this.eat(TokenTypes.String);
						this.eat(TokenTypes.CommandEndingBracket);

						// Slice odstraňuje ohraničující uvozovky řetězce
						path = $path.join(this.templateBaseDirectory, path.slice(1, -1));

						// Načtení souboru
						$fs.readFile(path, "utf-8", function (err, content) {
							if (err) {
								callback(err, null);
								return;
							}

							var parse = $path.parse(path);

							// Necháme vyřešit samostatným Parserem; není třeba řešit routovní (cesty includů) hlubších závislostí
							// Parser umí správně routovat v prvním includovaném souboru
							var parser = new Parser(content, parse.dir, parse.base);
							parser.parse(function (err, output) {
								callback(err, output);
							});
						});
					} else {
						callback(this.createError("Include expect identifier or file path."), null);
					}
				} catch (err) {
					callback(err, null);
				}
			}

			/**
    * Cmd FOR (of)
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdFor",
			value: function cmdFor(callback) {
				try {
					var variable = this.translateVariableName(this.token.value);

					this.eat(TokenTypes.Variable);

					if (this.token.value === "of" && this.token.type === TokenTypes.KeyWord) {
						this.cmdForOf(callback, variable);
						return;
					}

					// Normal for cycle
					var expr = this.processExpression(TokenTypes.CommandEndingBracket);
					this.eat(TokenTypes.CommandEndingBracket);

					if (this.tryToProcessEmptyBlock(KeyWords.For)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.For, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						// sestaven cyklu
						var output = '(function(){var __iterator=0;for(' + variable + expr + '){' + content + '__iterator++;}})();';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * CMD FOR OF
    * @param callback
    * @param item
    */

		}, {
			key: "cmdForOf",
			value: function cmdForOf(callback, item) {
				this.eat(TokenTypes.KeyWord);
				var array = this.translateVariableName(this.token.value);
				this.eat(TokenTypes.Variable);
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.For)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.For, function (err, content) {
					if (err) {
						callback(err, null);
						return;
					}

					// sestavení cyklu
					var output = '(function(){var __iterator=0,__itemKey,__lastIteration=Object.keys(' + array + ').length-1;for(' + item + ' in ' + array + ')' + '{if(!' + array + '.hasOwnProperty(' + item + '))continue;__itemKey=' + item + ";" + item + "=" + array + "[" + item + "];" + content + '__iterator++;}})();';

					callback(null, output);
				});
			}

			/**
    * Cmd IF - podmínka
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdIf",
			value: function cmdIf(callback) {
				try {
					var cond = this.processExpression(TokenTypes.CommandEndingBracket);
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.If)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.If, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = 'if(' + cond + '){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd VAR - store expression into variable
    * @private
    * @returns {string} output
    * @throws {Error}
    */

		}, {
			key: "cmdVar",
			value: function cmdVar() {
				var variable = this.translateVariableName(this.token.value);
				this.eat(TokenTypes.Variable);
				var expr = this.processExpression(TokenTypes.CommandEndingBracket);
				this.eat(TokenTypes.CommandEndingBracket);

				return variable + expr + ';';
			}

			/**
    * Cmd DEBUG - console.log given expression
    * @private
    * @returns {string} output
    * @throws {Error}
    */

		}, {
			key: "cmdDebug",
			value: function cmdDebug() {
				var variable = this.translateVariableName(this.token.value);
				this.eat(TokenTypes.Variable);
				this.eat(TokenTypes.CommandEndingBracket);
				return '__output+="<script>console.log(\'" + ' + variable + ' + "\');</script>";';
			}

			/**
    * Cmd LOC - localization
    * @private
    * @returns {string} output
    * @throws {Error}
    */

		}, {
			key: "cmdLoc",
			value: function cmdLoc() {
				var key = this.processExpression(TokenTypes.CommandEndingBracket);
				this.eat(TokenTypes.CommandEndingBracket);

				return '__output+=__loc("' + key.trim() + '");';
			}

			/**
    * Cmd FIRST - condtion - if first iteration of cycle
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdFirst",
			value: function cmdFirst(callback) {
				try {
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.First)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.First, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = 'if(__iterator==0){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd LAST - condtion - if last iteration of cycle
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdLast",
			value: function cmdLast(callback) {
				try {
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.Last)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.Last, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = 'if(__iterator==__lastIteration){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd EVEN - condtion - if iteration is even
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdEven",
			value: function cmdEven(callback) {
				try {
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.Even)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.Even, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = 'if(__iterator%2==0){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			/**
    * Cmd ODD - condtion - if iteration is odd
    * @private
    * @param {partialContentCallback} callback
    */

		}, {
			key: "cmdOdd",
			value: function cmdOdd(callback) {
				try {
					this.eat(TokenTypes.CommandEndingBracket);

					// Vyřešení prázdného bloku
					if (this.tryToProcessEmptyBlock(KeyWords.Odd)) {
						callback(null, "");
						return;
					}

					this.processBlock(KeyWords.Odd, function (err, content) {
						if (err) {
							callback(err, null);
							return;
						}

						var output = 'if(__iterator%2==1){' + content + '}';

						callback(null, output);
					});
				} catch (e) {
					callback(e);
				}
			}

			//</editor-fold>

			//</editor-fold>

		}]);

		return Parser;
	}();

	/**
  * List of helpers defined by user
  * @type {Object}
  */


	Parser.helpers = {};

	/**
  * List of block helpers defined by user
  * @type {Object}
  */
	Parser.blockHelpers = {};

	exports.Parser = Parser;
})(typeof window !== "undefined" ? window.Jumplate : module.exports, typeof module !== "undefined" && module.exports !== undefined && typeof require === "function");
