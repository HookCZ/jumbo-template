(function (exports, isNode) {
	var $fs, $path, LexicalAnalyser;

	if (isNode) {
		$fs = require("fs");
		$path = require("path");
	}

	LexicalAnalyser = isNode ? require("./LexicalAnalyser").LexicalAnalyser : exports.LexicalAnalyser;

	const TokenTypes = LexicalAnalyser.TokenTypes;
	const KeyWords = LexicalAnalyser.KeyWords;

	// Name of variable which is used to access key of iterated object in for in(/of)
	const iteratorName = "itemKey";

	/**
	 * Parser, class which do main part of compilation; it checks syntax too
	 */
	class Parser {
		//<editor-fold dest="Constructor">

		/**
		 * @param {string} templateContent Template code
		 * @param {string} templateBaseDirectory Base directory for template, used for file including
		 * @param {string} [fileName] emplate file name - used in errors
		 */
		constructor(templateContent, templateBaseDirectory, fileName) {
			/**
			 * @private
			 * @type {LexicalAnalyser}
			 */
			this.lexer = new LexicalAnalyser();

			/**
			 * Current token
			 * @private
			 * @type {Token|null}
			 */
			this.token = null;

			/**
			 * Offset obdržený z lexeru - offset, který se má nastavit, po přijetí současného tokenu
			 * @private
			 * @type {number}
			 */
			this.tokenOffset = 0;

			/**
			 * Vstupní kód šablony
			 * @private
			 */
			this.templateContent = templateContent;

			/**
			 * Offset od začátku kódu šablony
			 * @private
			 * @type {number}
			 */
			this.templateContentOffset = 0;

			/**
			 * Základní složka template pro řešení závislostí na souborech
			 * @private
			 * @type {string}
			 */
			this.templateBaseDirectory = templateBaseDirectory;

			/**
			 * Název souboru (šablony) pro upřesnění chybových hlášení
			 * @private
			 * @type {string | null}
			 */
			this.fileName = fileName || null;

			/**
			 * Count created cycles and provide unique identification of cycles when more cycles inside another
			 * @type {number}
			 */
			this.iteratorCounter = 0;

			// debug
			// this.lexer.doCompleteLexicalAnalyse(templateContent);
		}

		//</editor-fold>

		//<editor-fold dest="Virtual Callbacks">

		/**
		 * Virtual callback for method parse()
		 * @callback parseCallback
		 * @param {Error} error Chyba
		 * @param {string | null} compiledTemplate Zkompilovaná šablona
		 */

		/**
		 * Virtual callback for method processBlock()
		 * @callback partialContentCallback
		 * @param {Error} error Chyba
		 * @param {string | null} partialContent Část zkompilovného kódu
		 */

		//</editor-fold>

		//<editor-fold dest="Public Methods">

		/**
		 * Compile input and return compiled result
		 * @param {parseCallback} callback
		 */
		parse(callback) {
			this.processBlock(null, (err, output) => {
				callback(err, err ? null : output);
			});
		}

		//</editor-fold>

		//<editor-fold dest="Private Methods">

		/**
		 * Create error with given message
		 * @private
		 * @param {string} message
		 * @returns {Error}
		 */
		createError(message) {
			return new Error(message + " Unexpected token of type '"
				+ this.token.type + "' with value '" + this.token.value + "' found"
				+ (this.fileName ? (" in " + $path.join(this.templateBaseDirectory, this.fileName)) : "")
				+ " at line " + LexicalAnalyser.getLineNumberOfSymbolPosition(
					this.templateContentOffset,
					this.templateContent
				)
				+ ":" + LexicalAnalyser.getPositionOfSymbolInLine(
					this.templateContentOffset,
					this.templateContent)
				+ "."
			);
		}

		//noinspection JSMethodCanBeStatic
		/**
		 * Compile/translate variable name
		 * @private
		 * @param {string} variable
		 * @returns {string}
		 */
		translateVariableName(variable) {
			variable = variable.slice(1);

			// If it's $itemKey the iteration counter
			if (variable === iteratorName) {
				return '__itemKey';
			} else {
				var dotPos = variable.indexOf(".");
				var bracketPos = variable.indexOf("[");

				if (dotPos === -1 && bracketPos === -1) {
					return '__data["' + variable + '"]';
				}

				var baseEnd = (dotPos !== -1 && (dotPos < bracketPos || bracketPos === -1))
					? dotPos
					: bracketPos;

				return '__data["' + variable.slice(0, baseEnd) + '"]' + variable.slice(baseEnd);
			}
		}

		/**
		 * Helping procedure for getting token from input through lexical analyser
		 * @private
		 * @throws {Error}
		 */
		readToken() {
			// Pokud je offset vstupu roven offsetu tokenu,
			// tak je čtecí hlava připravena ke čtení dalšího tokenu,
			// jinak přerušíme - aby nedocházelo ke zbytečnému znovu-zpracování
			if (this.token != null && this.tokenOffset !== this.templateContentOffset) {
				return;
			}

			var err, token, offset;

			[err, token, offset] = this.lexer.getToken(this.templateContent, this.templateContentOffset);

			if (err) {
				err.message += (this.fileName ? (" in " + $path.join(this.templateBaseDirectory, this.fileName)) : "");
				throw err;
			}

			this.token = token;
			this.tokenOffset = offset;
		}

		/**
		 * Eat (remove) token from input and read new
		 * @private
		 * @param {LexicalAnalyser.TokenTypes} tokenType Expecting token type
		 * @throws {Error}
		 */
		eat(tokenType) {
			if (this.token.type === tokenType) {
				// Aktualizace offsetu - přesunutí kurzoru za poslední token
				this.templateContentOffset = this.tokenOffset;
				this.readToken();
				return;
			}

			throw this.createError("Token of type " + tokenType + " expected.");
		}

		/**
		 * Look for next token without moving input
		 * @private
		 * @returns {Token}
		 * @throws {Error}
		 */
		peek() {
			this.readToken();

			var err, token;
			[err, token] = this.lexer.getToken(this.templateContent, this.tokenOffset, true/*, this.token*/);

			if (err) {
				throw err;
			}

			return token;
		}

		/**
		 * Zpracuje blok příkazů; Tvoří hlavní rekurzivní cyklus pro zpracování šablony.
		 * @private
		 * @param {LexicalAnalyser.KeyWords | null} parent Identifikátor rodičovského bloku
		 * @param {partialContentCallback} callback
		 */
		processBlock(parent, callback) {

			// Proměnná obsahující výstup bloku
			var output = "";

			try {
				this.readToken();

				// HTML Context
				if (this.token.type === TokenTypes.HtmlContext) {
					if (this.token.value.trim().length > 0) {
						output += '__output+="'
							+ this.token.value./*replace(/(")|((?:\r|\n|(?:\r\n))+)|(\\\{)|(\\)/g, function(_, a,b,c,d) {
							if (a) return '\\"';
							else if (b) return "\\n";
							else if (c) return "{";
							else return "\\\\";
						})*/replace(/\\\{\{/g, "{{").replace(/\\/g, "\\\\")
								.replace(/(\r\n)|(\r|\n)+/g, "\\n").replace(/"/g, '\\"')
							+ '";';
					}
					this.eat(TokenTypes.HtmlContext);
				}
				// CMD
				else if (this.token.type === TokenTypes.CommandStartingBracket) {
					this.eat(TokenTypes.CommandStartingBracket);

					// Print variable (escaped)
					if (this.token.type === TokenTypes.Variable) {
						output += '__output+=_eHtml(' + this.translateVariableName(this.token.value) + ');';
						this.eat(TokenTypes.Variable);
						this.eat(TokenTypes.CommandEndingBracket);
					}
					// Unescaped variable print
					else if (this.token.type === TokenTypes.Negation) {
						this.eat(TokenTypes.Negation);
						let varName = this.token.value;
						this.eat(TokenTypes.Variable);
						output += '__output+=__out(' + this.translateVariableName(varName) + ');';
						this.eat(TokenTypes.CommandEndingBracket);
					}
					// Comment
					else if (this.token.type === TokenTypes.Comment) {
						this.eat(TokenTypes.Comment);
						this.eat(TokenTypes.CommandEndingBracket);
					} else {
						var keyword = this.token.value;

						this.eat(TokenTypes.KeyWord);

						// Callback for block cmds
						var blockCmdCallback = (err, contentOutput) => {
							if (err) {
								callback(err, null);
								return;
							}

							output += contentOutput;
							this.finalizeMacro(parent, callback, output);
						};

						switch (keyword) {
							case KeyWords.For:
								this.cmdFor(blockCmdCallback);
								return;
							case KeyWords.If:
								this.cmdIf(blockCmdCallback);
								return;
							case KeyWords.ElseIf:
								var cond = this.processExpression(TokenTypes.CommandEndingBracket);
								this.eat(TokenTypes.CommandEndingBracket);
								output += '}else if(' + cond + '){';
								break;
							case KeyWords.Else:
								output += '}else{';
								this.eat(TokenTypes.CommandEndingBracket);
								break;
							case KeyWords.First:
								this.cmdFirst(blockCmdCallback);
								return;
							case KeyWords.Last:
								this.cmdLast(blockCmdCallback);
								return;
							case KeyWords.Even:
								this.cmdEven(blockCmdCallback);
								return;
							case KeyWords.Odd:
								this.cmdOdd(blockCmdCallback);
								return;
							case KeyWords.Loc:
								output += this.cmdLoc();
								break;
							case KeyWords.Block:
								this.cmdBlock(blockCmdCallback);
								return;
							case KeyWords.Include:
								this.cmdInclude(blockCmdCallback);
								return;
							case KeyWords.Defined:
								this.cmdDefined(blockCmdCallback);
								return;
							case KeyWords.Define:
								this.cmdDefine(blockCmdCallback);
								return;
							case KeyWords.Var:
								output += this.cmdVar();
								break;
							case KeyWords.Debug:
								output += this.cmdDebug();
								break;
							default:
								// Validate if helper exists
								if (!(keyword in Parser.helpers) && !(keyword in Parser.blockHelpers)) {
									callback(new Error("Unsupported command '" + keyword + "' found at line "
										+ LexicalAnalyser.getLineNumberOfSymbolPosition(
											this.templateContentOffset,
											this.templateContent
										) + (this.fileName
												? (" in " + $path.join(this.templateBaseDirectory, this.fileName)) : ""
										) + "."), null);

									return;
								}

								if (keyword in Parser.helpers) {
									// Process helper
									output += this.helperCmd(keyword);
								} else {
									// Process block helper
									this.blockHelperCmd(keyword, blockCmdCallback);
									return;
								}
						}
					}
				}

				this.finalizeMacro(parent, callback, output);
			} catch (err) {
				callback(err, null);
			}
		}

		/**
		 * Finallize block macro
		 * @param parent
		 * @param callback
		 * @param output
		 */
		finalizeMacro(parent, callback, output) {
			try {
				this.readToken();

				// EOF - výstup z rekurze - vrácení finálního překladu
				if (parent == null && this.token.type === TokenTypes.EndOfFile) {
					this.eat(TokenTypes.EndOfFile);
					callback(null, output);
				} else {
					var nextToken = this.peek();

					// Pokud se jedná o konečný příkaz (existuje parent), který ukončuje současný kontext
					//noinspection JSValidateTypes
					if (parent != null && this.token.type === TokenTypes.CommandStartingBracket
						&& nextToken.type === TokenTypes.KeyWordEnding
						&& LexicalAnalyser.getEndKeyWordByStart(parent) === nextToken.value) {

						this.eat(TokenTypes.CommandStartingBracket);
						this.eat(TokenTypes.KeyWordEnding);
						this.eat(TokenTypes.CommandEndingBracket);

						callback(null, output);
					}
					// Parent block still continue
					else {
						this.processBlock(parent, (err, nextOutput) => {
							callback(err, output + nextOutput);
						});
					}
				}
			} catch (err) {
				callback(err, null);
			}
		}

		/**
		 * Parse expression
		 * @private
		 * @param {LexicalAnalyser.TokenTypes} endToken
		 * @returns {string} output
		 * @throws {Error}
		 */
		processExpression(endToken) {
			// TODO: rework reading expression - integrate to lexer - read from curent pos to indexOf endToken

			var output = "";
			while (this.token.type !== endToken && this.token.type !== TokenTypes.EndOfFile) {
				// console.log(this.token);
				if (this.token.type === TokenTypes.Variable) {
					output += this.translateVariableName(this.token.value);
				} else {
					output += this.token.value;
				}

				this.eat(this.token.type);
			}

			return output;
		}

		/**
		 * Naparsuje inkrementační výraz - změnu hodnoty proměnné v určitém kroku (eg. ++, +=, /=, etc..)
		 * @private
		 * @returns {string} output
		 * @throws {Error}
		 */
		processValuation() {
			var variable = this.token.value;

			this.eat(TokenTypes.Variable);

			variable = this.translateVariableName(variable);
			var operator = this.token.value;

			this.eat(TokenTypes.ArithmeticOperator);

			if (operator === "++" || operator === "--") {
				return variable + operator;
			}
			// += | -= | /= | *= | %=
			else if (this.token.type === TokenTypes.Assignment) {
				this.eat(TokenTypes.Assignment);
				var value;

				if (this.token.type === TokenTypes.Number) {
					value = this.token.value;
					this.eat(TokenTypes.Number);
				} else if (this.token.type === TokenTypes.Variable) {
					value = this.translateVariableName(this.token.value);
					this.eat(TokenTypes.Variable);
				} else {
					throw this.createError("Final expression can contain only number or variable.");
				}

				return variable + operator + value;
			}
		}

		/**
		 * Ověří, jestli je požadovaný blok prázdný, zpracuje jej, a vrátí true
		 * Pokud se nejedná o prázdný blok, nic neprovádí a vrací false
		 * @private
		 * @param {LexicalAnalyser.KeyWords} parent
		 * @returns {boolean}
		 * @throws {Error}
		 */
		tryToProcessEmptyBlock(parent) {
			var nextToken = this.peek();

			if (parent != null && this.token.type === TokenTypes.CommandStartingBracket
				&& nextToken.type === TokenTypes.KeyWordEnding
				&& LexicalAnalyser.getEndKeyWordByStart(parent) === nextToken.value) {

				this.eat(TokenTypes.CommandStartingBracket);
				this.eat(TokenTypes.KeyWordEnding);
				this.eat(TokenTypes.CommandEndingBracket);

				return true;
			}

			return false;
		}

		//<editor-fold desc="Command's implementation">

		/**
		 * Process user's custom helper command
		 * @param {String} helper
		 */
		helperCmd(helper) {
			this.eat(TokenTypes.LeftBracket);
			var expr = this.processExpression(TokenTypes.RightBracket);
			this.eat(TokenTypes.RightBracket);
			this.eat(TokenTypes.CommandEndingBracket);

			return '__output+=__hlprs["' + helper + '"].call(__context,' + expr + ');';
		}

		/**
		 * Process user's custom block helper command
		 * @param {String} helper Helper's name / keyword
		 * @param callback
		 */
		blockHelperCmd(helper, callback) {
			try {
				if (this.token.type === TokenTypes.LeftBracket) {
					this.eat(TokenTypes.LeftBracket);
					var expr = this.processExpression(TokenTypes.RightBracket);
					this.eat(TokenTypes.RightBracket);
				}

				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(helper)) {
					callback(null, "");
					return;
				}

				this.processBlock(helper, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					// add block content
					var output = '__output+=__bhlprs["' + helper + '"].call(__context, (function(){var __output="";' + content
						+ 'return __output;})(), ' + expr + ');';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd DEFINE - store code for later usage, not print out resul in place
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdDefine(callback) {
			try {
				var id = this.token.value;

				this.eat(TokenTypes.Identifier);
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.Define)) {
					// Empty block should exists too
					callback(null, '__blocks["' + id + '"]="";');
					return;
				}

				this.processBlock(KeyWords.Define, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = '__blocks["' + id + '"]=(function(){var __output="";' + content + 'return __output;})();';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd DEFINED - Ověření existence definice; v případě že existuje, vypíše se obsah bloku
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdDefined(callback) {
			try {
				var id = this.token.value;

				this.eat(TokenTypes.Identifier);
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.Defined)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.Defined, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					// if (definitions exists) add content to output
					var output = 'if(__blocks["' + id + '"]){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd BLOCK - uložení kódu pro pozdější použítí + v místě definice se nevypisuje
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdBlock(callback) {
			try {
				var id = this.token.value;

				this.eat(TokenTypes.Identifier);
				this.eat(TokenTypes.CommandEndingBracket);

				if (this.tryToProcessEmptyBlock(KeyWords.Block)) {
					// Empty block should exists too
					callback(null, '__blocks["' + id + '"]="";');
					return;
				}

				this.processBlock(KeyWords.Block, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = '__output+=__blocks["' + id + '"]=(function(){var __output="";' + content + 'return __output;})();';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd INCLUDE - vypíše obsah bloku (pokud přijímá identifikátor) nebo načte soubor (pokud přijímá řetězec)
		 * @private
		 * @param {partialContentCallback} callback
		 * @returns {string} output
		 */
		cmdInclude(callback) {
			try {
				if (this.token.type === TokenTypes.Identifier) {
					var id = this.token.value;

					this.eat(TokenTypes.Identifier);
					this.eat(TokenTypes.CommandEndingBracket);

					callback(null, '__output+=(__blocks["' + id + '"] || "");');
				} else if (this.token.type === TokenTypes.String) {
					if (!isNode) throw new Error("Importing file is impossible on client!");

					var path = this.token.value;

					this.eat(TokenTypes.String);
					this.eat(TokenTypes.CommandEndingBracket);

					// Slice odstraňuje ohraničující uvozovky řetězce
					path = $path.join(this.templateBaseDirectory, path.slice(1, -1));

					// Načtení souboru
					$fs.readFile(path, "utf-8", (err, content) => {
						if (err) {
							callback(err, null);
							return;
						}

						var parse = $path.parse(path);

						// Necháme vyřešit samostatným Parserem; není třeba řešit routovní (cesty includů) hlubších závislostí
						// Parser umí správně routovat v prvním includovaném souboru
						var parser = new Parser(content, parse.dir, parse.base);
						parser.parse((err, output) => {
							callback(err, output);
						});
					});
				} else {
					callback(this.createError("Include expect identifier or file path."), null);
				}
			} catch (err) {
				callback(err, null);
			}
		}

		/**
		 * Cmd FOR (of)
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdFor(callback) {
			try {
				var variable = this.translateVariableName(this.token.value);

				this.eat(TokenTypes.Variable);

				if (this.token.value === "of" && this.token.type === TokenTypes.KeyWord) {
					this.cmdForOf(callback, variable);
					return;
				}

				// Normal for cycle
				var expr = this.processExpression(TokenTypes.CommandEndingBracket);
				this.eat(TokenTypes.CommandEndingBracket);

				if (this.tryToProcessEmptyBlock(KeyWords.For)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.For, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					// sestaven cyklu
					var output = '(function(){var __iterator=0;for(' + variable + expr
						+ '){' + content + '__iterator++;}})();';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * CMD FOR OF
		 * @param callback
		 * @param item
		 */
		cmdForOf(callback, item) {
			this.eat(TokenTypes.KeyWord);
			var array = this.translateVariableName(this.token.value);
			this.eat(TokenTypes.Variable);
			this.eat(TokenTypes.CommandEndingBracket);

			// Vyřešení prázdného bloku
			if (this.tryToProcessEmptyBlock(KeyWords.For)) {
				callback(null, "");
				return;
			}

			this.processBlock(KeyWords.For, (err, content) => {
				if (err) {
					callback(err, null);
					return;
				}

				// sestavení cyklu
				var output = '(function(){var __iterator=0,__itemKey,__lastIteration=Object.keys(' + array
					+ ').length-1;for(' + item + ' in ' + array + ')'
					+ '{if(!' + array + '.hasOwnProperty(' + item + '))continue;__itemKey=' + item + ";"
					+ item + "=" + array + "[" + item + "];" + content + '__iterator++;}})();';

				callback(null, output);
			});
		}

		/**
		 * Cmd IF - podmínka
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdIf(callback) {
			try {
				var cond = this.processExpression(TokenTypes.CommandEndingBracket);
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.If)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.If, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = 'if(' + cond + '){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd VAR - store expression into variable
		 * @private
		 * @returns {string} output
		 * @throws {Error}
		 */
		cmdVar() {
			var variable = this.translateVariableName(this.token.value);
			this.eat(TokenTypes.Variable);
			var expr = this.processExpression(TokenTypes.CommandEndingBracket);
			this.eat(TokenTypes.CommandEndingBracket);

			return variable + expr + ';';
		}

		/**
		 * Cmd DEBUG - console.log given expression
		 * @private
		 * @returns {string} output
		 * @throws {Error}
		 */
		cmdDebug() {
			var variable = this.translateVariableName(this.token.value);
			this.eat(TokenTypes.Variable);
			this.eat(TokenTypes.CommandEndingBracket);
			return '__output+="<script>console.log(\'" + ' + variable + ' + "\');</script>";';
		}

		/**
		 * Cmd LOC - localization
		 * @private
		 * @returns {string} output
		 * @throws {Error}
		 */
		cmdLoc() {
			var key = this.processExpression(TokenTypes.CommandEndingBracket);
			this.eat(TokenTypes.CommandEndingBracket);

			return '__output+=__loc("' + key.trim() + '");';
		}

		/**
		 * Cmd FIRST - condtion - if first iteration of cycle
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdFirst(callback) {
			try {
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.First)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.First, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = 'if(__iterator==0){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd LAST - condtion - if last iteration of cycle
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdLast(callback) {
			try {
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.Last)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.Last, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = 'if(__iterator==__lastIteration){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd EVEN - condtion - if iteration is even
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdEven(callback) {
			try {
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.Even)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.Even, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = 'if(__iterator%2==0){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		/**
		 * Cmd ODD - condtion - if iteration is odd
		 * @private
		 * @param {partialContentCallback} callback
		 */
		cmdOdd(callback) {
			try {
				this.eat(TokenTypes.CommandEndingBracket);

				// Vyřešení prázdného bloku
				if (this.tryToProcessEmptyBlock(KeyWords.Odd)) {
					callback(null, "");
					return;
				}

				this.processBlock(KeyWords.Odd, (err, content) => {
					if (err) {
						callback(err, null);
						return;
					}

					var output = 'if(__iterator%2==1){' + content + '}';

					callback(null, output);
				});
			} catch (e) {
				callback(e);
			}
		}

		//</editor-fold>

		//</editor-fold>
	}

	/**
	 * List of helpers defined by user
	 * @type {Object}
	 */
	Parser.helpers = {};

	/**
	 * List of block helpers defined by user
	 * @type {Object}
	 */
	Parser.blockHelpers = {};

	exports.Parser = Parser;
})((typeof window !== "undefined") ? window.Jumplate : module.exports, typeof module !== "undefined" && module.exports !== undefined && typeof require === "function");