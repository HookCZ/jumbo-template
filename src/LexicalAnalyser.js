(function (exports) {
	/**
	 * Definition of Virtual type Token
	 * @typedef {{type: LexicalAnalyser.TokenTypes, value: String}} Token
	 */

	// Private variables for saving often accessed data
	var _startingSymbolSearchRegExp, _startingSymbolRegExp, TokenTypes, _startingSymbol, _terminalMapArray, _endingSymbolRegExp;
	var _startToEndKeyWordMap = new Map();

	var _keyWords = [
		"defined", "define", "block", "include",
		"var",
		"for", "in", "of",
		"elseif", "if", "else", // elseif must be before else and if
		"first", "last", "even", "odd",
		"loc",
		"debug"
	];

	var _keyWordsEndings = [
		"/define", "/defined", "/block",
		"/for",
		"/if",
		"/first", "/last", "/even", "/odd"
	];

	const symbolForEscape = ["{", "}", ".", "$", "^", "(", ")"];
	function escapeSymbol(symbol) {
		var out = "";
		for (var l of symbol) {
			if (symbolForEscape.indexOf(l) != -1) {
				out += "\\" + l;
			} else {
				out += l;
			}
		}

		return out;
	}

	/**
	 * This class do lexical analyse over template code
	 */
	class LexicalAnalyser {

		//<editor-fold desc="Enums">

		/**
		 *
		 * @returns {{Defined: string, Define: string, Block: string, Include: string, Var: string, For: string, If: string, ElseIf: string, Else: string, Loc: string, First: string, Last: string, Even: string, Odd: string, Debug: string}}
		 * @enum
		 */
		static get KeyWords() {
			//noinspection JSValidateTypes
			return {
				Defined: "defined",
				Define: "define",
				Block: "block",
				Include: "include",
				Var: "var",
				For: "for",
				If: "if",
				ElseIf: "elseif",
				Else: "else",
				Loc: "loc",
				First: "first",
				Last: "last",
				Even: "even",
				Odd: "odd",
				Debug: "debug"
			};
		}

		/**
		 * @returns {{EndDefined: string, EndDefine: string, EndBlock: string, EndFor: string, EndIf: string, EndFirst: string, EndLast: string, EndEven: string, EndOdd: string}}
		 * @@enum
		 */
		static get EndKeyWords() {
			return {
				EndDefined: "/defined",
				EndDefine: "/define",
				EndBlock: "/block",
				EndFor: "/for",
				EndIf: "/if",
				EndFirst: "/first",
				EndLast: "/last",
				EndEven: "/even",
				EndOdd: "/odd"
			};
		}

		/**
		 * @enum
		 */
		static get TokenTypes() {
			//noinspection JSValidateTypes
			return {
				/** @type {LexicalAnalyser.TokenTypes} */
				HtmlContext: "HtmlContext",
				/** @type {LexicalAnalyser.TokenTypes} */
				CommandStartingBracket: "CommandStartingBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				CommandEndingBracket: "CommandEndingBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				Variable: "Variable",
				/** @type {LexicalAnalyser.TokenTypes} */
				KeyWord: "KeyWord",
				/** @type {LexicalAnalyser.TokenTypes} */
				KeyWordEnding: "KeyWordEnding",
				/** @type {LexicalAnalyser.TokenTypes} */
				String: "String",
				/** @type {LexicalAnalyser.TokenTypes} */
				Number: "Number",
				/** @type {LexicalAnalyser.TokenTypes} */
				Boolean: "Boolean",
				/** @type {LexicalAnalyser.TokenTypes} */
				Object: "Object",
				/** @type {LexicalAnalyser.TokenTypes} */
				ArrayLeftBracket: "ArrayLeftBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				ArrayRightBracket: "ArrayRightBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				Comma: "Comma",
				/** @type {LexicalAnalyser.TokenTypes} */
				Colon: "Colon",
				/** @type {LexicalAnalyser.TokenTypes} */
				Semicolon: "Semicolon",
				/** @type {LexicalAnalyser.TokenTypes} */
				Dot: "Dot",
				/** @type {LexicalAnalyser.TokenTypes} */
				WhiteSpace: "WhiteSpace",
				/** @type {LexicalAnalyser.TokenTypes} */
				Comment: "Comment",
				/** @type {LexicalAnalyser.TokenTypes} */
				Assignment: "Assignment",
				/** @type {LexicalAnalyser.TokenTypes} */
				ArithmeticOperator: "ArithmeticOperator",
				/** @type {LexicalAnalyser.TokenTypes} */
				Comparison: "Comparison",
				/** @type {LexicalAnalyser.TokenTypes} */
				Identifier: "Identifier",
				/** @type {LexicalAnalyser.TokenTypes} */
				EndOfFile: "EndOfFile",
				/** @type {LexicalAnalyser.TokenTypes} */
				ClassIdentifier: "ClassIdentifier",
				/** @type {LexicalAnalyser.TokenTypes} */
				LeftBracket: "LeftFunctionBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				RightBracket: "RightBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				FunctionCall: "FunctionCall",
				/** @type {LexicalAnalyser.TokenTypes} */
				Negation: "Negation",
				/** @type {LexicalAnalyser.TokenTypes} */
				ObjectStartingBracket: "ObjectStartingBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				ObjectEndingBracket: "ObjectEndingBracket",
				/** @type {LexicalAnalyser.TokenTypes} */
				LogicalOperator: "LogicalOperator",
			};
		}

		//</editor-fold>

		//<editor-fold desc="Static Properties">

		/**
		 * Symbol used for command start
		 * @return {string}
		 */
		static get startingSymbol() {
			return "{{";
		}

		/**
		 * Symbol used for command end
		 * @return {string}
		 */
		static get endingSymbol() {
			return "}}";
		}

		/**
		 * RegExp for starting symbol
		 * Symbol "{", not foregoing by escape
		 * @private
		 * @type {RegExp}
		 */
		static get startingSymbolSearchRegExp() {
			if (!_startingSymbolSearchRegExp) {
				_startingSymbolSearchRegExp = new RegExp("(?:[^\\\\]|^)(" + escapeSymbol(LexicalAnalyser.startingSymbol) + ")");
			}
			return _startingSymbolSearchRegExp;
			// ! String.search() match not-matching group !
		}

		/**
		 * RegExp for starting symbol
		 * @private
		 * @type {RegExp}
		 */
		static get startingSymbolRegExp() {
			if (!_startingSymbolRegExp) {
				_startingSymbolRegExp = new RegExp("^" + escapeSymbol(LexicalAnalyser.startingSymbol));
			}
			return _startingSymbolRegExp;
		}

		/**
		 * RegExp for ending symbol
		 * @private
		 * @type {RegExp}
		 */
		static get endingSymbolRegExp() {
			if (!_endingSymbolRegExp) {
				_endingSymbolRegExp = new RegExp("^" + escapeSymbol(LexicalAnalyser.endingSymbol));
			}
			return _endingSymbolRegExp;
		}

		/**
		 * List of keywords
		 * @return {string[]}
		 */
		static get keyWords() {
			return _keyWords;
		}

		/**
		 * List of ending keywords
		 * @return {string[]}
		 */
		static get keyWordsEndings() {
			return _keyWordsEndings;
		}

		/**
		 * Define Terminals and their regular expressions for matching
		 * @private
		 */
		static get terminals() {
			if (!_terminalMapArray) {
				var map = [];

				// Commet
				map.push({terminal: TokenTypes.Comment, regex: new RegExp("^(\\*[\\s\\S]*?\\*)")});

				// Sort by keyword length desc
				//
				function orderByLengthDesc(a, b) {
					if (a.length > b.length) return -1;
					if (a.length < b.length) return 1;
					return 0;
				}

				// Ending keywords
				var keyWordsTerminal = "^(";
				var endKeywords = LexicalAnalyser.keyWordsEndings;
				endKeywords.sort(orderByLengthDesc);
				var il = endKeywords.length;
				for (let i = 0; i < il; i++) {
					keyWordsTerminal += "(?:" + endKeywords[i] + ")";

					if (i != il - 1) {
						keyWordsTerminal += "|";
					}
				}
				keyWordsTerminal += ")" + LexicalAnalyser.endingSymbol;
				map.push({terminal: TokenTypes.KeyWordEnding, regex: new RegExp(keyWordsTerminal)});

				// Keywords
				keyWordsTerminal = "^(";
				var keywords = LexicalAnalyser.keyWords;
				keywords.sort(orderByLengthDesc);
				il = keywords.length;
				for (let i = 0; i < il; i++) {
					keyWordsTerminal += "(?:" + keywords[i] + ")";

					if (i != il - 1) {
						keyWordsTerminal += "|";
					}
				}
				keyWordsTerminal += ")";
				map.push({terminal: TokenTypes.KeyWord, regex: new RegExp(keyWordsTerminal)});

				map.push({regex: new RegExp("^ [A-Z][a-zA-Z]*"), terminal: TokenTypes.ClassIdentifier}); // Před whitespace
				map.push({regex: new RegExp("^\\s+"), terminal: TokenTypes.WhiteSpace});
				map.push({
					regex: new RegExp("^\\$[a-z][a-zA-Z0-9]*(?:(?:\\[\"(?:(?:[^\"\\\\])*|(?:\\\\\"?)*)*\"\\])|(?:\\.[a-zA-Z][a-zA-Z0-9]*))*"),
					terminal: TokenTypes.Variable
				});
				// map.push({regex: new RegExp("^\\.[a-z][a-zA-Z0-9]*\\(((,)?)*\\)"), terminal:  TokenTypes.FunctionCall});
				map.push({regex: _startingSymbolRegExp, terminal: TokenTypes.CommandStartingBracket});
				map.push({regex: _endingSymbolRegExp, terminal: TokenTypes.CommandEndingBracket});
				map.push({regex: new RegExp("^\\{"), terminal: TokenTypes.ObjectStartingBracket});
				map.push({regex: new RegExp("^\\}"), terminal: TokenTypes.ObjectEndingBracket});
				map.push({regex: new RegExp("^\"(?:(?:[^\"\\\\])*|(?:\\\\\"?)*)*\""), terminal: TokenTypes.String});
				map.push({regex: new RegExp("^(?:\\+|-)?[0-9]+(?:\\.[0-9]+)?"), terminal: TokenTypes.Number});
				map.push({regex: new RegExp("^(?:(?:true)|(?:false))"), terminal: TokenTypes.Boolean});
				map.push({regex: new RegExp("^="), terminal: TokenTypes.Assignment});
				map.push({regex: new RegExp("^;"), terminal: TokenTypes.Semicolon});
				map.push({regex: new RegExp("^,"), terminal: TokenTypes.Comma});
				map.push({regex: new RegExp("^:"), terminal: TokenTypes.Colon});
				map.push({
					regex: new RegExp("^((?:&)|(?:\\|)|(?:\\^))"),
					terminal: TokenTypes.LogicalOperator
				});
				map.push({
					regex: new RegExp("^((?:\\+\\+)|(?:--)|(?:\\+)|(?:-)|(?:\\*)|(?:/)|(?:%))"),
					terminal: TokenTypes.ArithmeticOperator
				});
				map.push({
					regex: new RegExp("^((?:>)|(?:<)|(?:==)|(?:===)|(?:!=)|(?:!==)|(?:>=)|(?:<=))"),
					terminal: TokenTypes.Comparison
				});

				// Identifier must be after keywords
				map.push({regex: new RegExp("^[a-z][a-zA-Z]*"), terminal: TokenTypes.Identifier});

				// Last
				map.push({regex: new RegExp("^\\("), terminal: TokenTypes.LeftBracket});
				map.push({regex: new RegExp("^\\)"), terminal: TokenTypes.RightBracket});
				map.push({regex: new RegExp("^\\."), terminal: TokenTypes.Dot});
				map.push({regex: new RegExp("^!"), terminal: TokenTypes.Negation});
				map.push({regex: new RegExp("^\\["), terminal: TokenTypes.ArrayLeftBracket});
				map.push({regex: new RegExp("^\\]"), terminal: TokenTypes.ArrayRightBracket});

				_terminalMapArray = map;
			}

			return _terminalMapArray;
		}

		//</editor-fold>

		//<editor-fold desc="Constructor">

		/**
		 * Constructor
		 */
		constructor() {
			/**
			 * Hold last toen
			 * @type {Token|null}
			 */
			this.lastToken = null;
		}

		//</editor-fold>

		//<editor-fold desc="Static Methods">

		/**
		 * Register helper to lexer
		 * @param {String} name
		 */
		static registerHelper(name) {
			_keyWords.push(name);
		}

		/**
		 * Register block helper to lexer
		 * @param {String} name
		 */
		static registerBlockHelper(name) {
			_keyWords.push(name);
			_keyWordsEndings.push("/" + name);
			// Map keyword startto its end
			_startToEndKeyWordMap.set(name, "/" + name);
		}

		/**
		 * Return ending keyword for given keyword
		 * @param {LexicalAnalyser.KeyWords} start Keyword identifying block
		 * @returns {LexicalAnalyser.EndKeyWords}
		 */
		static getEndKeyWordByStart(start) {
			return _startToEndKeyWordMap.get(start);
		}

		/**
		 * Return line for given symbol position
		 * @param {Number} pos
		 * @param {string} input
		 * @return {Number}
		 */
		static getLineNumberOfSymbolPosition(pos, input) {
			return input.substr(0, pos).split(/\r?\n/).length;
		}

		/**
		 * Return position of symbol in its line
		 * @param {Number} pos
		 * @param {string} input
		 * @return {Number}
		 */
		static getPositionOfSymbolInLine(pos, input) {
			var lastPos = 0;

			// Nalezení pozic všech \r\n zneužitím rozšířeného replace
			input.slice(0, pos).replace(/\r?\n/g, function (match, index) {
				lastPos = index + match.length;
			});

			return pos - lastPos;
		}

		/**
		 * Search first token in the input
		 * @private
		 * @param input
		 * @param inputOffset
		 * @returns {[ Error, Token, Number ]}
		 */
		static searchForToken(input, inputOffset) {
			var match, matchFound, terminal = null;
			var inputSubstr = input.slice(inputOffset);
			var offset = inputOffset;

			// Test each terminal
			var terminals = LexicalAnalyser.terminals;
			for (var i = 0; i < terminals.length; i++) {
				match = inputSubstr.match(terminals[i].regex);
				if (match != null) {
					terminal = terminals[i].terminal;
					matchFound = true;
					break;
				}
			}

			// Error - unexpected symbol
			if (!matchFound) {
				return [new Error("Unexpected '" + inputSubstr.slice(0, 20) + "' found at line "
					+ LexicalAnalyser.getLineNumberOfSymbolPosition(inputOffset, input)), null, null];
			}

			// If match-group exists use it, or use default match
			match = match[1] || match[0];
			offset += match.length;

			return [null, {
				type: terminal,
				value: match
			}, offset];
		}

		//</editor-fold>

		//<editor-fold desc="Public Methods">

		/**
		 * Return Token which is first in input
		 * @param {string} input Template code
		 * @param {number} inputOffset Offset Offset of already processed code in input
		 * @param {boolean} [peek] Mark as peek call - will not change holded last token
		 * @returns {[Error, Token, Number]}
		 */
		getToken(input, inputOffset, peek) {
			if (typeof input != "string") {
				return [new Error("Invalid argument input. No input given."), null, inputOffset];
			}

			if (inputOffset >= input.length) {
				return [null, {
					type: TokenTypes.EndOfFile,
					value: null
				}, inputOffset];
			}

			var index;
			var lastToken = this.lastToken;

			if (lastToken == null || lastToken.type == TokenTypes.CommandEndingBracket
				|| lastToken.type == TokenTypes.HtmlContext
			) {
				input = input.slice(inputOffset);
				index = input.search(_startingSymbolSearchRegExp);

				var indexFixed = false;

				// Pokud byl nalezen startovací symbol na začátku (index == 0), ale nejedná se o první symbol
				// +1 je fix chyby string.search(), která nebere v potaz not-matching group
				if (index == 0 && input[0] != _startingSymbol[0]) {
					index++;
					indexFixed = true;
				}

				if (index != 0) {
					var substring = index == -1 ? input : input.slice(0, index + !indexFixed);
					lastToken = {
						type: TokenTypes.HtmlContext,
						value: substring
					};

					if (!peek) this.lastToken = lastToken;

					return [null, lastToken, inputOffset + substring.length];
				} else {
					lastToken = {
						type: TokenTypes.CommandStartingBracket,
						value: _startingSymbol
					};

					if (!peek) this.lastToken = lastToken;

					return [null, lastToken, inputOffset + _startingSymbol.length];
				}
			} else {
				var error, token, offset;

				[error, token, offset] = LexicalAnalyser.searchForToken(input, inputOffset);

				if (error) {
					return [error, null, offset];
				}

				if (!peek) this.lastToken = token;

				if (token.type == TokenTypes.WhiteSpace) {
					[error, token, offset] = LexicalAnalyser.searchForToken(input, offset);
				}

				return [error, token, offset];
			}
		}

		/**
		 * Just for debug - print list of all found tokens to console
		 * @private
		 */
		doCompleteLexicalAnalyse(input) {
			var err, token, offset = 0;
			[err, token, offset] = this.getToken(input, offset);

			while (token.type != TokenTypes.EndOfFile) {
				[err, token, offset] = this.getToken(input, offset);
			}

			// Reinicializace - vrácení instance do původního stavu
			this.lastToken = null;
		}

		//</editor-fold>

	}

	TokenTypes = LexicalAnalyser.TokenTypes;
	_startingSymbolSearchRegExp = LexicalAnalyser.startingSymbolSearchRegExp;
	_startingSymbolRegExp = LexicalAnalyser.startingSymbolRegExp;
	_startingSymbol = LexicalAnalyser.startingSymbol;
	_endingSymbolRegExp = LexicalAnalyser.endingSymbolRegExp;

	// Create map of keywords paired to their ends
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Block, LexicalAnalyser.EndKeyWords.EndBlock);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.If, LexicalAnalyser.EndKeyWords.EndIf);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Define, LexicalAnalyser.EndKeyWords.EndDefine);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Defined, LexicalAnalyser.EndKeyWords.EndDefined);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.For, LexicalAnalyser.EndKeyWords.EndFor);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.First, LexicalAnalyser.EndKeyWords.EndFirst);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Last, LexicalAnalyser.EndKeyWords.EndLast);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Even, LexicalAnalyser.EndKeyWords.EndEven);
	_startToEndKeyWordMap.set(LexicalAnalyser.KeyWords.Odd, LexicalAnalyser.EndKeyWords.EndOdd);

	exports.LexicalAnalyser = LexicalAnalyser;
})((typeof window !== "undefined") ? window.Jumplate : module.exports);