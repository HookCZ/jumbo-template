(function (exports, isNode) {
	var $fs, $path;

	if (isNode) {
		$fs = require("fs");
		$path = require("path");
	}

	var SPEC_CHAR_REGEX = /[\x26\x3c\x22\x27\x3e]/g;

	/**
	 * Class which load Jumplate's template code and compile it.
	 * Compiled template can be taken and saved for future use.
	 * After compile then it can be rendered into HTML with given set of data.
	 */
	class Jumplate {

		//<editor-fold desc="Constructor">

		/**
		 * Construct Jumplate; You should let Jumplate loads template files on its own
		 * if you call constructor like new Jumplate(null, "path") or new Jumplate(null, "path", null, "layout path")
		 * @param {String} template Template content
		 * @param {String} [templatePath] Path to template, used for resolving includes
		 * @param {String} [layout] Layout template content
		 * @param {String} [layoutPath] Path to layout template, used for resolving includes
		 * @throws {Error}
		 */
		constructor(template, templatePath = null, layout = null, layoutPath = null) {
			/**
			 * @private
			 * @type {null}
			 */
			this.localizator = null;

			/**
			 * @private
			 * @type {null}
			 */
			this.helpers = {};

			// If it's called from static method ::fromCache() - do not continue with initialization
			if (arguments.length === 0) {
				return;
			}

			// For client, only first two params allowed
			if (!isNode) {
				layout = templatePath;
				templatePath = null;
				layoutPath = null;
			}

			if ((template !== null || templatePath === null)
				&& (typeof template != "string" || template.toString().trim().length == 0)
			) {
				throw new Error("Invalid template");
			}

			if (templatePath !== null && (typeof templatePath != "string" || templatePath.toString().trim().length == 0)) {
				throw new Error("Invalid template path");
			}

			if (layout !== null && (typeof layout != "string" || layout.toString().trim().length == 0)) {
				throw new Error("Invalid layout");
			}

			if (layoutPath !== null && (typeof layoutPath != "string" || layoutPath.toString().trim().length == 0)) {
				throw new Error("Invalid layout path");
			}

			if (templatePath != null) {
				templatePath = $path.resolve(templatePath);
			}

			if (layoutPath != null) {
				layoutPath = $path.resolve(layoutPath);
			}

			/**
			 * @private
			 * @type {String}
			 */
			this.template = template;

			/**
			 * @private
			 * @type {String}
			 */
			this.templatePath = templatePath;

			/**
			 * @private
			 * @type {String}
			 */
			this.layout = layout;

			/**
			 * @private
			 * @type {String}
			 */
			this.layoutPath = layoutPath;

			/**
			 * Field for code of compiled template
			 * @private
			 * @type {string}
			 */
			this.output = "";

			/**
			 * Field for code of compiled template's layout
			 * @private
			 * @type {string}
			 */
			this.layoutOutput = "";

			/**
			 * Your optional context under which will be helpers called ("this" will point to context)
			 * @type {Object}
			 */
			this.context = null;
		}

		//</editor-fold>

		//<editor-fold desc="Virtual Callbacks">

		/**
		 * Virtual callback for render() method
		 * @callback renderCallback
		 * @param {Error} error Error
		 * @param {string | null} htmlOutput Output HTML
		 */

		/**
		 * Virtual callback for compile() method
		 * @callback compileCallback
		 * @param {Error} error Error
		 * @param {string | null} template Compiled template code
		 */

		//</editor-fold>

		//<editor-fold desc="Static Methods">

		/**
		 * Returns instance of Jumplate with precompiled code, just ready for render()
		 * @param {String} cachedTemplate Content of previously compiled template
		 * @returns {Jumplate}
		 */
		static fromCache(cachedTemplate) {
			let jumplate = new Jumplate();
			jumplate.output = cachedTemplate;

			return jumplate;
		}

		/**
		 * Register function which will be called for each {loc} command; Localization key will be given as parameter
		 * @param localizationHandler
		 * @throws
		 */
		static registerLocalizator(localizationHandler) {
			if (typeof localizationHandler !== "function") {
				throw new Error("Parameter must be function");
			}

			Jumplate.localizator = localizationHandler;
		}

		/**
		 * Register helper
		 * @param {String} name
		 * @param {Function} helper
		 * @throws {Error}
		 */
		static registerHelper(name, helper) {
			// Check if helper's name is reserved Jumplate macro
			if (Jumplate.LexicalAnalyser.keyWords.indexOf(name) != -1) {
				throw new Error(`You are registering helper '${name})' but this name is reserved.`);
			} else if (typeof helper != "function") {
				throw new Error(`Your helper '${name}' is not a function.`);
			} else if (Jumplate.Parser.blockHelpers.hasOwnProperty(name)) {
				throw new Error(`Helper '${name} is already registered as block helper.`);
			}

			Jumplate.Parser.helpers[name] = helper;
			Jumplate.LexicalAnalyser.registerHelper(name);
		}

		/**
		 * Register block helper
		 * @param {String} name
		 * @param {Function} helper
		 * @throws {Error}
		 */
		static registerBlockHelper(name, helper) {
			// Check if helper's name is reserved Jumplate macro
			if (Jumplate.LexicalAnalyser.keyWords.indexOf(name) != -1) {
				throw new Error(`You are registering helper '${name})' but this name is reserved.`);
			} else if (typeof helper != "function") {
				throw new Error(`Your helper '${name}' is not a function.`);
			} else if (Jumplate.Parser.helpers.hasOwnProperty(name)) {
				throw new Error(`Helper '${name} is already registered as non-block helper.`);
			}

			Jumplate.Parser.blockHelpers[name] = helper;
			Jumplate.LexicalAnalyser.registerBlockHelper(name);
		}

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Set default locale code which should be used for toLocaleString; instance setLocale overrides this.
		 * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
		 */
		static setLocale(localeCode) {
			// Commented out for performance reason. Developer should know if he set valid locale.
			// if (!/^[a-z]{2}-[A-Z]{2}$/.test(localeCode)) {
			// 	throw new Error("Invalid locale code.");
			// }

			Jumplate.locale = localeCode;
		}

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Set default timezone which should be used for toLocaleString; instance setTimeZone overrides this.
		 * @param {string} timeZone
		 */
		static setTimeZone(timeZone) {
			Jumplate.timeZone = timeZone;
		}

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Escape HTML in given input
		 * @param input
		 * @returns {string | void | *}
		 */
		static escape(input) {
			if (input == undefined) { // match null too
				return "";
			}

			return input.replace(SPEC_CHAR_REGEX, function (char) {
				return "&#" + char.charCodeAt(0) + ";"
			});
		}

		//</editor-fold>

		//<editor-fold desc="Public Methods">

		/**
		 * Compile input template
		 * @param {compileCallback} callback
		 */
		compile(callback) {
			if (typeof this.template == "undefined") {
				// Template was created from cached data; No compile possible
				return;
			}

			var templateDone = false, layoutDone = false, noLayout = this.layoutPath == null && this.layout == null;
			// noLayout will change default BLOCK behavior; if layout set, block will not produce output
			// it'll wait for include

			var checkIfBothDone = () => {
				if (noLayout) {
					callback(null, this.output);
					return;
				}

				if (templateDone && layoutDone) {
					var output = '(function(){let __output="";' + this.output + '})();' + this.layoutOutput;
					this.output = output;
					callback(null, output);
				}
			};

			var someErrorOccursAlready = false;

			var templateFileCallback = (err, templateContent) => {
				if (someErrorOccursAlready) return;

				if (err) {
					someErrorOccursAlready = true;
					err.message = "[Template Error] " + err.stack;
					callback(err, null);
					callback = () => { };
					return;
				}

				var parse = isNode ? $path.parse(this.templatePath || "") : {dir: "", base: ""};

				// Kompilace šablony
				var parser = new Jumplate.Parser(templateContent, parse.dir, parse.base);
				parser.parse((err, template) => {
					if (someErrorOccursAlready) return;

					if (err) {
						someErrorOccursAlready = true;
						err.message = "[Template Error] " + err.stack;
						callback(err, null);
						return;
					}

					this.output = template;
					templateDone = true;

					checkIfBothDone();
				});
			};

			// Načte šablonu
			if (this.template == null) {
				$fs.readFile(this.templatePath, "utf8", templateFileCallback);
			} else {
				templateFileCallback(null, this.template);
			}

			// Načte layout
			if (!noLayout) {
				var layoutFileCallback = (err, layoutContent) => {
					if (someErrorOccursAlready) return;

					if (err) {
						someErrorOccursAlready = true;
						err.message = "[Layout Error] " + err.stack;
						callback(err, null);
						return;
					}

					var parse = isNode ? $path.parse(this.templatePath || "") : {dir: "", base: ""};

					// Kompilace layoutu
					var layoutParser = new Jumplate.Parser(layoutContent, parse.dir, parse.base);
					layoutParser.parse((err, layout) => {
						if (someErrorOccursAlready) return;

						if (err) {
							someErrorOccursAlready = true;
							err.message = "[Layout Error] " + err.stack;
							callback(err, null);
							return;
						}

						this.layoutOutput = layout;
						layoutDone = true;

						checkIfBothDone();
					});
				};

				if (this.layout == null) {
					$fs.readFile(this.layoutPath, "utf8", layoutFileCallback);
				} else {
					layoutFileCallback(null, this.layout);
				}
			}
		}

		/**
		 * Render compiled template with given set of data
		 * @param {Object} data
		 * @param {renderCallback} callback
		 */
		render(data, callback) {
			if (data.constructor != Object) {
				throw new Error("Invalid input data");
			}

			try {
				// Local variables which are used with compiled code in eval()
				//noinspection JSUnusedLocalSymbols
				var __data = data, __output = "", __defines = {}, __blocks = {},
					__hlprs = Jumplate.Parser.helpers, __bhlprs = Jumplate.Parser.blockHelpers,
					__context = this.context, _locale = this.locale || Jumplate.locale,
					_timezone = this.timeZone || Jumplate.timeZone,
					__loc = (function(key) {
						return Jumplate.localizator(key, _locale);
					}) || (function (key) {
						return key;
					});

				function _eHtml(input) {
					// noinspection EqualityComparisonWithCoercionJS
					if (input == undefined) {
						return "";
					}

					// noinspection EqualityComparisonWithCoercionJS
					if (input.constructor === Date) {
						return input.toLocaleString(_locale, { timeZone: _timezone });
					}

					// noinspection EqualityComparisonWithCoercionJS
					return input.toLocaleString(_locale).replace(SPEC_CHAR_REGEX, function (char) {
						return "&#" + char.charCodeAt(0) + ";"
					});
				}

				function __out(input) {
					if (input == undefined) {
						return "";
					}

					return input.toLocaleString(_locale);
				}

				eval(this.output);

				callback(null, __output);
			} catch (e) {
				callback(e, null);
			}
		}

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Set locale code which should be used for toLocaleString when printing variables
		 * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
		 */
		setLocale(localeCode) {
			// Commented out for performance reason. Developer should know if he set valid locale.
			// if (!/^[a-z]{2}-[A-Z]{2}$/.test(localeCode)) {
			// 	throw new Error("Invalid locale code.");
			// }

			this.locale = localeCode;
		}

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Set timezone which should be used for toLocaleString when printing Date
		 * @param {string} timeZone
		 */
		setTimeZone(timeZone) {
			this.timeZone = timeZone;
		}

		//</editor-fold>
	}

	if (isNode) {
		Jumplate.LexicalAnalyser = require("./LexicalAnalyser").LexicalAnalyser;
		Jumplate.Parser = require("./Parser").Parser;
	}

	exports.Jumplate = Jumplate;
})((typeof window !== "undefined") ? window : module.exports,
	typeof module !== "undefined" && module.exports !== undefined && typeof require === "function");