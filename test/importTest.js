const assert = require("assert");

describe("Module import", function() {
	it("Jumplate should exists", function() {
		const {Jumplate} = require("../index");
		assert.equal(!!Jumplate, true);
	});

	it("Jumplate's constructor should be Jumplate", function() {
		const {Jumplate} = require("../index");
		assert.equal(Jumplate.prototype.constructor, Jumplate);
	});

	it("Jumplate should contains LexicalAnalyser", function() {
		const {Jumplate} = require("../index");
		assert.equal(Jumplate.LexicalAnalyser.prototype.constructor, Jumplate.LexicalAnalyser);
	});

	it("Jumplate should contains Parser", function() {
		const {Jumplate} = require("../index");
		assert.equal(Jumplate.Parser.prototype.constructor, Jumplate.Parser);
	});
});