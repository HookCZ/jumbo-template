export class Jumplate
{
	constructor(template: string, templatePath?: string, layout?: string, layoutPath?: string);

	/**
	 * Create Jumplate instance from cached precompiled template
	 * @param {string} cachedTemplate
	 * @return {Jumplate}
	 */
	static fromCache(cachedTemplate: string): Jumplate;

	/**
	 * Register localization handler
	 * @param {(locKey: string) => string} localizationHandler
	 */
	static registerLocalizator(localizationHandler: (locKey: string) => string): void;

	/**
	 * Register custom helper
	 * @param name
	 * @param helper
	 */
	static registerHelper(name: string, helper: (...args: any[]) => string): void;

	/**
	 * Register custom block helper
	 * @param name
	 * @param helper
	 */
	static registerBlockHelper(name: string, helper: (blockContent: string, ...args: any[]) => string): void;

	/**
	 * Set default locale code which should be used for toLocaleString; instance setLocale overrides this.
	 * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
	 */
	static setLocale(localeCode);

	/**
	 * Set default timezone which should be used for toLocaleString; instance setTimeZone overrides this.
	 * @param {string} timeZone
	 */
	static setTimeZone(timeZone);

	/**
	 * Escape HTML in given input
	 * @param input
	 * @returns {string | void | *}
	 */
	static escape(input);

	/**
	 * Compile template in Jumplate instance
	 * @param {(err: Error, compileResult: string) => void} callback
	 */
	compile(callback: (err: Error, compileResult: string) => void): void;

	/**
	 * Render template
	 * @param data
	 * @param {(err: Error, renderResult: string) => void} callback
	 */
	render(data, callback: (err: Error, renderResult: string) => void): void;

	/**
	 * Set locale code which should be used for toLocaleString when printing variables
	 * @param {string} localeCode Locale in format locale-CODE eg. 'en-US'
	 */
	setLocale(localeCode: string): void;

	/**
	 * Set timezone which should be used for toLocaleString when printing Date
	 * @param {string} timeZone
	 */
	setTimeZone(timeZone: string): void;
}