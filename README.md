# Jumplate (jumbo-template) #

Jumplate is one of the fastest (faster than Jade, EJS, Handlebars, Underscore) Node.js template engine with cacheable precompilation.
Jumplate is able to compile 16 000 templates in one second and render 400 000 precompiled templates in one second upon one 2,33 GHz core.

Template code is compiled to native JavaScript (can be cached),
than it can be rendered with given set of data into final HTML.

### Created for [JumboJS](https://www.npmjs.com/package/jumbo-core) framework
Writen in ES6 for Node.js. Babeled for client-side execution. Should run in most recent browsers and IE11 (older should work too but it's not guaranteed).

# Usage
Example usages of Jumplate.

## Basic Usage
Creating instance of Jumplate with given template code.

Layout example
```html
<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Task manager for managing project development. Work with backlog and tasks in multiple project. Created as DEMO for JumboJS framework.">
	<meta name="keywords" content="JumboJS, task, project, backlog, manager">
	<meta name="author" content="Roman Jámbor">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="icon" href="/public/favicon.ico">
	<link rel="stylesheet" href="/public/styles/default.css">

	<script src="/public/uJumbo/ujumbo.js"></script>
	<script src="/public/scripts/ujtest.js"></script>

	<title>Node Task Manager | JumboJS DEMO</title>

	{{include head}}
</head>
<body>
	<div class='header{{if $homepage}} homepage{{/if}}'>
		<header>
			<h1>Node Task Manager</h1>
			<h2>Welcome, thanks for giving it a try</h2>
		</header>
	</div>
	
	<main data-j-snippet="content">
		{{include content}}
	</main>
</body>
</html>
```

Partial template example
```html
{{block content}}
	<h1>Homepage</h1>
	<h2>{{$subtitle}}</h2>

	{{if $user.age < 18}}
		{{include "../not-allowed.jshtml"}}
	{{/if}}

	{{* Declare variable inside template *}}
	{{var $date = new Date()}}
	Current time: <b>{{$date}}</b>

	{{* Log $date into browser console *}}
	{{debug $date}}

	<ul>
		{{for $item of $items}}
			<li>
				{{$item.name}} (My key is: {{$itemKey}})
				{{last}}
					and I'm the last one
				{{/last}}
			</li>
		{{/for}}
	</ul>

	<section>
		{{!$htmlStyledContent}}
	</section>
{{/block}}
```

```javascript
Jumplate.registerLocalizator(function(key) {
    if (key == "Home.index.text") {
        return "Example localized text";
    }
    
    return key;
});

var tpl = new Jumplate(
    "<body><h1>{{$title}}</h1>{{loc Home.index.text}}</body>");

tpl.compile((error, compiledTemplate) => {
    if (error) {
        console.error(error.message);
        return;
    }
    
    // compiledTemplate can be saved to cache here
    
    tpl.render(/* Data */{
        title: "Example usage"
    }, (error, htmlOutput) => {
        if (error) {
            console.error(error.message);
            return;
        }
        
        // You can return htmlOutput to client
        console.log(htmlOutput);
    })
});
```

## With Layout
```javascript
var template = "{{block content}}<h1>{{define title}}{{$title}} with layout{{/define}}</h1>"
    + "{{loc Home.index.text}}{{/block}}";
var layout = "<html><head><title>Example{{defined title}} | {{use title}}{{/defined}}</title>"
    + "</head><body>{{include content}}</body></html>";

var tpl = new Jumplate(template, null, layout);
tpl.compile((error, compiledTemplate) => {
    // ... same ...
});
```

## With File Include
```javascript
var template = '{{block content}}<h1>{{$title}}</h1>'
    + '{{loc Home.index.text}}{{include "footer.tpl"}}{{/block}}';

var tpl = new Jumplate(template, "path/to/template.tpl"); 
// Path is used as base dir for resolving include
// and file name is used in errors to specify in which file error occurs
tpl.compile((error, compiledTemplate) => {
    // ... same ...
});
```

## Let Jumplate Load Templates
```javascript
var tpl = new Jumplate(null, "path/to/template.tpl", null, "path/to/layout.tpl"); 
// Path is used as base dir for resolving include
// and file name is used in errors to specify in which file error occurs
tpl.compile((error, compiledTemplate) => {
    // ... same ...
});
```

## Use Cached Template
```javascript
var tpl = Jumplate.fromCache("compiled cached template code");
tpl.render((error, htmlOutput) => {
    // ...
});
```


# Ability Overview

## Commands
### Variables
All variables begin with `$` symbol.
Each variable can be printed as `{{$variable}}`. Output has escaped html entities (<, >, &, ", ').
If you want to print variable without escaping use `{{!$variable}}`.

It's possible to define variable right inside 
template with `{{var $x = new Date().toString()}}`

### Block
Define block of code which is rendered in place of definition. 
Can be reused by `{{include blockName}}`. 
Block can be included many times.
```
{{block blockName}}Block content. Will be rendered here{{/block}}
```

### Define
Define is similar to block, but define will not be rendered in place of definition.
```
{{define defineBlockName}}Define block content{{/define}}
```

Both `define` and `block` can be checked by `{{defined defineBlockName}}Block with name defineBlockName exists.{{/defined}}`.


### Include
Include block (define too) or other template file.

`{{include blockName}}` will include block with given name.

`{{include "./path/to/template.tpl"}}` will include file with given path; relative to current file.

### Cycles
There are two cycles. 
```
{{for $x = 0; $x < $count; $x++}}For content{{/for}}
```
and 
```
{{for $item of $list}}Foreach content{{/foreach}}
```
In case of for-of there is special variable `$itemKey` which is accessible 
inside cycle block and contains key of current item.

In both cycles you can use commands 
```
{{first}}Will be rendered if this iteration is first{{/first}}
```
```
{{last}}Will be rendered if this iteration is last{{/last}}
```
```
{{even}}Will be rendered if this iteration is even{{/even}}
```
```
{{odd}}Will be rendered if this iteration is odd{{/odd}}
```

### Conditions
```
{{if $x == "condition"}}
    if condition true...
{{elseif $x == "else if condition"}}
    if else if condition true...
{{else}}
    else...
{{/if}}
```

### Localization
You can register localization hadler to Jumplate and 
than use command `{{loc Key.for.requested.translation}}` which will call your handler
with given key as parameter.

```javascript
var tpl = new Jumplate(...);

// Register own handler
tpl.registerLocalizator(function(key) {
    // Do whatever you want with your key and return translation
})
```

Jumplate has static methods `setLocale` and `setTimeZone` to set default locale and timezone which are used in .toLocaleString for Dates eg.
Same methods exist on instance too. Those methods override the static ones.

## Helpers
You can define own helpers, inline and block too. On class Jumplate
there are static methods `registerHelper(name, func)` and `registerBlockHelper(name, func)`.

```javascript
Jumplate.registerHelper("link", function(text, url) {
    return `<a href="${url}">${text}</a>`;
});
```
Then you can use it in template like this.
```
{{block menu}}
    {{foreach $item in $menuItems}}
        {{link($item.text, $item.url)}}
    {{/foreach}}
{{/block}}
```

# TODO
- Allow prototype function calling when printing variable eg. `{{$createdOn.format("d.m.Y")}}`
